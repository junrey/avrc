<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(!isset($_SESSION[getSystemName()]['usercode'])) {
	echo "<div class='error_message'>$error_display_no_username</div>";
	exit();
}

// insert item
if(isset($_GET["barcode"]) && trim($_GET["barcode"]) != "") {
	$barcode = escapeString($_GET['barcode']);

	$q = mysql_query("SELECT barcode FROM borrow_item WHERE void = '0' AND item_return = '0' AND usercode = '".getSessionVar('usercode')."' AND barcode = '$barcode'") or die(mysql_error());
	if(mysql_num_rows($q) == 0) {
		$q2 = mysql_query("SELECT code, barcode FROM inv_item WHERE phaseout = '0' AND barcode = '$barcode'") or die(mysql_error());
		if(mysql_num_rows($q2) > 0) {
			while($r2= mysql_fetch_assoc($q2)) {
				try {
		            begin();
					$itemcode = $r2['code'];
					mysql_query("INSERT INTO borrow_item (itemcode, barcode, qty, void, usercode, borrow_date) VALUES ('$itemcode', '$barcode', '1', '0', '".getSessionVar('usercode')."', NOW())") or die(mysql_error());
					commit();
		        } catch(Exception $e) {
		            rollback();
		            exit();
		        }
			}
		} else {
			echo "<div class='error_message'>Item not found!</div>";
		}
	}
}

// void item / remove
if(isset($_GET["void_barcode"]) && trim($_GET["void_barcode"]) != "") {
	$void_barcode = escapeString($_GET['void_barcode']);

	$q = mysql_query("SELECT barcode FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."' AND barcode = '$void_barcode'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
		try {
			begin();
			mysql_query("DELETE FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."' AND barcode = '$void_barcode'") or die(mysql_error());
			//mysql_query("UPDATE borrow_item SET void = '1', void_date = NOW() WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."' AND barcode = '$void_barcode'") or die(mysql_error());
			commit();
		} catch(Exception $e) {
			rollback();
			exit();
		}
	} else {
		echo "<div class='error_message'>Item not found!</div>";
	}
}

// void all / remove all
if(isset($_GET["void_all"]) && trim($_GET["void_all"]) != "") {
	$q = mysql_query("SELECT barcode FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
		try {
			begin();
			mysql_query("DELETE FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."'") or die(mysql_error());
			//mysql_query("UPDATE borrow_item SET void = '1', void_date = NOW() WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."'") or die(mysql_error());
			commit();
		} catch(Exception $e) {
			rollback();
			exit();
		}
	} else {
		echo "<div class='error_message'>Item not found!</div>";
	}
}

$q = mysql_query("SELECT itemcode, barcode, qty, void, usercode, borrow_date FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."' ORDER BY borrow_date DESC") or die(mysql_error());
?>
<script>
	$(function() {
		$().w2destroy("item");
		$('#item').w2grid({
			name: 'item',
			multiSelect : false,
			show : {
				header      : false,
				lineNumbers : true
			},
			columns: [
				{ field: 'barcode', caption: 'Barcode', size: '20%' },
				{ field: 'desc',caption: 'Description',  size: '40%'},
				{ field: 'borrow_date', caption: 'Borrow Date ', size: '25%' }
			],
			records: [
				<?php while($r = mysql_fetch_assoc($q)) { ?>
					<?php
						$desc = "";
						$q2 = mysql_query("SELECT description FROM inv_item WHERE barcode = '$r[barcode]'") or die(mysql_error());
						$r2 = mysql_fetch_assoc($q2);
						$desc = htmlentities($r2['description'], ENT_QUOTES);
					?>
					{ recid: '<?php echo $r['barcode']; ?>', barcode: '<?php echo $r['barcode']; ?>', desc: '<?php echo $desc; ?>', borrow_date: '<?php echo datetime("m/j/y", $r['borrow_date']); ?><?php echo datetime("h:i:s a", $r['borrow_date']); ?>' },
				<?php } ?>
			],
			onDblClick: function(event) {
				event.onComplete = function () {
					var sel = w2ui['item'].getSelection();
					var recid = sel[0];
					$('#barcode').val(recid).focus();
				}
			}
		});
	});
</script>
