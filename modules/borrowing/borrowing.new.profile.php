<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

// pre-configure settings

mysql_query("SET NAMES utf8"); //display enye

if(isset($_GET['borrower']) && trim($_GET['borrower']) != "") {
	$borrower = escapeString($_GET['borrower']);
	$image    = '<img src="images/no_profile_pic.png" />';
	
	if (file_exists("../../student/" . $borrower . ".jpg")) {
		$image   = '<img src="../../student/' . $borrower . '.jpg" />';
	} else if (file_exists("../../student/" . $borrower . ".jpeg")) {
		$image   = '<img src="../../student/' . $borrower . '.jpeg" />';
	} else if (file_exists("../../faculty/" . $borrower . ".jpg")) {
			$image   = '<img src="../../faculty/' . $borrower . '.jpg" />';
	} else if (file_exists("../../faculty/" . $borrower . ".jpeg")) {
		$image   = '<img src="../../faculty/' . $borrower . '.jpeg" />';
	}


	echo  $image;

}
?>
