<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

// pre-configure settings

mysql_query("SET NAMES utf8"); //display enye

if(isset($_GET['borrower']) && trim($_GET['borrower']) != "") {
	$borrower = escapeString($_GET['borrower']);
	$display  = "";
	$image    = '<img src="images/no_profile_pic.gif" />';
	
	$q = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$borrower'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
		$r = mysql_fetch_assoc($q);
		$display = setUTF8String($r['fname'] . " " . $r['lname']);
		
		if (file_exists("../../student/" . $borrower . ".jpg")) {
			$image   = '<img src="../../student/' . $borrower . '.jpg" />';
		} else if (file_exists("../../student/" . $borrower . ".jpeg")) {
			$image   = '<img src="../../student/' . $borrower . '.jpeg" />';
		}

	} else {
		$q2 = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$borrower'") or die(mysql_error());
		if(mysql_num_rows($q2) > 0) {
			$r2 = mysql_fetch_assoc($q2);
			$display = setUTF8String($r2['fname'] . " " . $r2['lname']);
			
			if (file_exists("../../faculty/" . $borrower . ".jpg")) {
				$image   = '<img src="../../faculty/' . $borrower . '.jpg" />';
			} else if (file_exists("../../faculty/" . $borrower . ".jpeg")) {
				$image   = '<img src="../../faculty/' . $borrower . '.jpeg" />';
			}
		} 

	}

	if(strlen($display) > 40) $display = substr($display,0,40) . "...";

	echo '<div style="float:left; margin-right:10px;">' . $image . '</div><div style="float:left;"><div>ID Number: ' . $borrower . '</div><div>Name: ' . strtoupper($display) . '</div></div>';

}
?>
