<div id="grid_item_search" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>

<script>
	$(function () {
		$().w2destroy("grid_item_search");
		$('#grid_item_search').w2grid({
			name: 'grid_item_search',
			header: 'Search Item',
            multiSelect : false,
			url: '<?php echo $path; ?>/json.borrowing.item.search.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
			},
			columns: [
				{ field: 'barcode', caption: 'Barcode', size: '20%' },
				{ field: 'desc', caption: 'Description', size: '60%' },
				{ field: 'status', caption: 'Borrowed', size: '20%' },

			],
			multiSearch: false,
			searches: [
				{ field: 'barcode', caption: 'Barcode', type: 'text' },
				{ field: 'desc', caption: 'Description', type: 'text' }
			],
			onDblClick: function(event) {
				event.onComplete = function () {
					var sel = w2ui['grid_item_search'].getSelection();
					var recid = sel[0];
					$('#barcode').val(recid).focus();
				}
			}
		});
	});
</script>
