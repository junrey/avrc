<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(isset($_GET["borrower"]) && trim($_GET["borrower"]) != "") {
	$borrower = escapeString($_GET['borrower']);
	setUTF8();
	$q = mysql_query("SELECT idnumber, lname, fname FROM student WHERE CONCAT(idnumber, lname) LIKE '%$borrower%' LIMIT 10") or die(mysql_error());
	$q2 = mysql_query("SELECT idnumber, lname, fname FROM teacher WHERE CONCAT(idnumber, lname) LIKE '%$borrower%' LIMIT 10") or die(mysql_error());

?>
<div id="grid" style="width: 100%; height: 100%;"></div>

<script>
$(function() {
	$().w2destroy("grid");
	$('#grid').w2grid({
        name: 'grid',
		multiSelect : false,
		show : {
			lineNumbers: true
		},
        columns: [
            { field: 'idnumber', caption: 'ID Number', size: '10%' },
            { field: 'lname',caption: 'Last Name',  size: '10%'},
            { field: 'fname', caption: 'First Name', size: '10%' }
        ],
        records: [
			<?php while($r = mysql_fetch_assoc($q)) { ?>
            	{ recid: '<?php echo $r['idnumber']; ?>', idnumber: '<?php echo htmlentities($r['idnumber'], ENT_QUOTES); ?>', lname: '<?php echo htmlentities($r['lname'], ENT_QUOTES); ?>', fname: '<?php echo htmlentities($r['fname'], ENT_QUOTES); ?>' },
        	<?php } ?>
			<?php while($r2 = mysql_fetch_assoc($q2)) { ?>
            	{ recid: '<?php echo $r2['idnumber']; ?>', idnumber: '<?php echo htmlentities($r2['idnumber'], ENT_QUOTES); ?>', lname: '<?php echo htmlentities($r2['lname'], ENT_QUOTES); ?>', fname: '<?php echo htmlentities($r2['fname'], ENT_QUOTES); ?>' },
        	<?php } ?>
		],
		onDblClick: function(event) {
			event.onComplete = function () {
				var sel = w2ui['grid'].getSelection();
				var recid = sel[0];
				$('#borrower').val(recid).focus();
			}
		}
    });
});
</script>
<?php } ?>
