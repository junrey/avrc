<?php session_start();

require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(isset($_GET['code']) && trim($_GET['code']) != "" && isset($_GET['borrower']) && trim($_GET['borrower']) != "") {

	$code     = escapeString($_GET['code']);
	$borrower = escapeString($_GET['borrower']);

	$receipt_header = "";
	$q = mysql_query("SELECT receipt_header FROM config");
	$r = mysql_fetch_assoc($q);
	$receipt_header = $r['receipt_header'];


?>
    <div><?php echo $receipt_header . "<br /><br />"; ?></div>
    <div>
    <?php
		setUTF8();
        $cnt = 0;
        $q = mysql_query("SELECT borrow_header, usercode, subject, venue, teacher, remarks, barcode, borrow_date, return_date FROM borrow_item WHERE void = '0' AND borrow_header = '$code' ORDER BY borrow_date DESC") or die(mysql_error());
		while($r = mysql_fetch_assoc($q)) {
            if($cnt == 0) {
                echo "TRANS ID: " . $r['borrow_header'] . "<br />";

                $subject = setUTF8String($r['subject']);
                $venue   = setUTF8String($r['venue']);
                $teacher = setUTF8String($r['teacher']);

                if($subject != "" && strtoupper($subject) != "NONE") { echo "SUBJECT: " . $subject . "<br />"; }
                if($venue != "" && strtoupper($venue) != "NONE") { echo "VENUE: " . $venue . "<br />"; }
                if($teacher != "" && strtoupper($teacher) != "NONE") { echo "TEACHER: " . $teacher . "<br />"; }

                echo "DATE: " . datetime("m/j/y h:i:s a", $r['borrow_date']) . "<br />";
                echo "RETURN: " . datetime("m/j/y h:i:s a", $r['return_date']) . "<br />";
                echo "<div style='text-align:center;'>** ********** I T E M S ********** **</div><br />";
            }

            $cnt++;

            $description = "";
            $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
			$r2 = mysql_fetch_assoc($q2);
            $description = $r2['description'];

            echo "BARCODE: " . $r['barcode'] . "<br />";
            echo "ITEM: " . $description . "<br />";
        }

        echo "<br />";

        $q = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$borrower'") or die(mysql_error());
		if(mysql_num_rows($q) > 0) {
			$r = mysql_fetch_assoc($q);

			echo "<font color='#FF0000'>BORROWER: " . strtoupper($r['fname']  . " " . $r['lname']) . "</font><br />";

		} else {
            $q = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$borrower'") or die(mysql_error());
			if(mysql_num_rows($q) > 0) {
				$r = mysql_fetch_assoc($q);

				echo "<font color='#FF0000'>BORROWER: " . strtoupper($r['fname'] . " " . $r['lname']) . "</font><br />";

			} else {
                echo "<font color='#FF0000'>BORROWER: $borrower" . "</font><br />";
            }

        }

        echo "<font color='#FF0000'>SERVED BY: " . getSessionVar('username') . "</font><br />";
    ?>
    <br /><br />
    </div>
    <div style="text-align:center;">****************************************</div>
	<?php /* <script>
		$(function() {
			window.print();
		});
	</script> */ ?>
<?php } ?>
