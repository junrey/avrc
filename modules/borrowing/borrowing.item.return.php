<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(isset($_GET["borrower"]) && trim($_GET["borrower"]) != "") {
	$borrower = escapeString($_GET['borrower']);

	// query for pending items
	$q_a = mysql_query("SELECT itemcode, barcode, usercode, borrow_date FROM borrow_item WHERE void = '0' AND item_return = '0' AND borrower = '$borrower' ORDER BY borrow_date DESC") or die(mysql_error());
	if(mysql_num_rows($q_a) > 0) {
?>

	<script>
		$(function() {
			$().w2destroy("item");
			$('#item').w2grid({
				header: 'Pending item(s) needs to return.',
		        name: 'item',
				multiSelect : false,
				show : {
					header      : true,
					lineNumbers : true
				},
		        columns: [
		            { field: 'barcode', caption: 'Barcode', size: '20%' },
					{ field: 'desc',caption: 'Description',  size: '40%'},
		            { field: 'duty', caption: 'Duty', size: '20%' },
					{ field: 'borrow_date', caption: 'Borrow Date', size: '25%' }
		        ],
		        records: [
					<?php while($r_a = mysql_fetch_assoc($q_a)) { ?>
						<?php
							$desc = "";
							$q2_a = mysql_query("SELECT description FROM inv_item WHERE barcode = '$r_a[barcode]'") or die(mysql_error());
							$r2_a = mysql_fetch_assoc($q2_a);
							$desc = htmlentities($r2_a['description'], ENT_QUOTES);
						?>
		            	{ recid: '<?php echo $r_a['barcode']; ?>', barcode: '<?php echo $r_a['barcode']; ?>', desc: '<?php echo $desc; ?>', duty: '<?php echo getUserLogName($r_a['usercode']); ?>', borrow_date: '<?php echo datetime("m/j/y", $r_a['borrow_date']); ?> <?php echo datetime("h:i:s a", $r_a['borrow_date']); ?>', style: 'background-color: #FF3333' },
		        	<?php } ?>
				],
				onDblClick: function(event) {
					event.onComplete = function () {
						var sel = w2ui['item'].getSelection();
						var recid = sel[0];
						$('#barcode').val(recid).focus();
					}
				}
		    });
		});
	</script>
	<?php } ?>
<?php } ?>
