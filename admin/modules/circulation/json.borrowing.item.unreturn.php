<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

$date_from = date('Y') . "-" . date('m') . "-" . date('d');
$date_to   = date('Y') . "-" . date('m') . "-" . date('d');

if(isset($_GET['date_from']) && trim($_GET['date_from']) != "") {
	$date_from = date("Y-m-d", strtotime(trim($_GET['date_from'])));
}

if(isset($_GET['date_to']) && trim($_GET['date_to']) != "") {
	$date_to = date("Y-m-d", strtotime(trim($_GET['date_to'])));
}

$limit = "100";
if(isset($_POST['limit'])){
    $limit = escapeString($_POST['limit']);
}

$offset = "0";
if(isset($_POST['offset'])){
    $offset = escapeString($_POST['offset']);
}

$search_field = "";
if(isset($_POST['search'][0]['field'])){
    $search_field = escapeString($_POST['search'][0]['field']);
}

$search_value = "";
if (isset($_POST['search'][0]['value'])){
    $search_value = escapeString($_POST['search'][0]['value']);
}

setUTF8();

$q  = mysql_query("SELECT SQL_CALC_FOUND_ROWS * FROM borrow_item WHERE borrow_header <> '' AND void = '0' AND item_return <> '1' AND (DATE(borrow_date) BETWEEN '$date_from' AND '$date_to') ORDER BY borrow_date DESC LIMIT $offset, $limit") or die(mysql_error());
$q2 = mysql_query("SELECT FOUND_ROWS() AS total") or die(mysql_error());
$r2 = mysql_fetch_assoc($q2);

$total_records = $r2['total'];
?>

<?php

echo "{ \"total\": $total_records, \"records\": [";
	$cnt = 0;
	while($r = mysql_fetch_assoc($q)) {
		$cnt++;
		if($cnt > 1) {
			echo ",";
		}
		
		$borrower = "";
		$q2 = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$r[borrower]'");
		if(mysql_num_rows($q2) > 0) {
			$r2 = mysql_fetch_assoc($q2);
			$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

		} else {
			$q2 = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$r[borrower]'");
			if(mysql_num_rows($q2) > 0) {
				$r2 = mysql_fetch_assoc($q2);
				$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

			} else {
				$borrower = $r['borrower'];
			}

		}

        $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);


       	echo "{ \"recid\": \"$r[itemcode]\", \"borrow_date\": \"" . datetime("m/j/y h:i:s a", $r['borrow_date']) . "\", \"borrower_id\": \"$r[borrower]\", \"borrower_name\": \"" . cleanString($borrower) . "\", \"teacher\": \"" . cleanString($r['teacher']) . "\", \"subject\": \"" . cleanString($r['subject']) . "\", \"remarks\": \"" . cleanString($r['remarks']) . "\", \"barcode\": \"$r[barcode]\", \"item\": \"" . cleanString($r2['description']) . "\", \"venue\": \"" . cleanString($r['venue']) . "\", \"staff\": \"" . cleanString(getUserLogName($r['usercode'])) . "\" }";
   }

echo " ] }";

?>
