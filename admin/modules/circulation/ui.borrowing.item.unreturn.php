<div id="toolbar" style="padding: 4px; border:1px solid silver;"></div>
<div id="grid" style="width:100%; height:calc(100% - 40px);"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#toolbar').w2toolbar({
			name : 'toolbar',
		 	items: [
				{ type: 'html', html: 'Date From: <input type=\"text\" name=\"date_from\" id=\"date_from\">' },
				{ type: 'html', html: '&nbsp;' },
				{ type: 'html', html: 'Date To: <input type=\"text\" name=\"date_to\" id=\"date_to\">' },
				{ type: 'button', id: 'load', caption: 'Display', img: 'icon-page' },
				{ type: 'button', id: 'excel_form', caption: 'Excel Form', img: 'icon-page' },
			],
			onClick: function (target, data) {
				if (target == 'load') {
					w2ui['grid'].load( '<?php echo $path; ?>/modules/circulation/json.borrowing.item.unreturn.php?date_from=' + $('#date_from').val() + '&date_to=' + $('#date_to').val());
				} else if (target == 'excel_form') {
					document.location.href='<?php echo $path; ?>/modules/report/xls.report.circulation.borrowing.item.unreturn.php?date_from=' + $('#date_from').val() + '&date_to=' + $('#date_to').val();

				}
			}
		});
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/circulation/json.borrowing.item.unreturn.php',
			show: {
				header        : true,
				toolbar       : false,
				footer        : true,
				lineNumbers   : true,
			},
			columns: [

				{ field: 'borrow_date', caption: 'Borrowed Date', size: '18%' },
				{ field: 'borrower_id', caption: 'ID Number', size: '8%' },
				{ field: 'borrower_name', caption: 'Borrower\'s Name', size: '20%' },
				{ field: 'teacher', caption: 'Teacher', size: '10%' },
				{ field: 'subject', caption: 'Subject', size: '10%' },
				{ field: 'remarks', caption: 'Remarks', size: '10%' },
				{ field: 'barcode', caption: 'Barcode', size: '10%' },
				{ field: 'item', caption: 'Item', size: '10%' },
				{ field: 'venue', caption: 'Venue', size: '10%' },
				{ field: 'staff', caption: 'Served By', size: '10%' },

			],
			multiSearch: false,
			searches: [
				{ field: 'borrower_id', caption: 'ID Number', type: 'text' },
			]
		});
		
		$('#date_from').w2field('date');
		$('#date_to').w2field('date');
	});
</script>
