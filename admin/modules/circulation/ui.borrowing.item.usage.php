<div id="grid" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/circulation/json.borrowing.item.usage.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
			},
			toolbar: {
	            items: [
	                { type: 'button', id: 'excel_form', caption: 'Excel Form', img: 'icon-page' }
	            ],
	            onClick: function (target, data) {
	                if (target == 'excel_form') {
	                    document.location.href='<?php echo $path; ?>/modules/report/xls.report.circulation.borrowing.item.usage.php';

	                }
	            }
	        },
			columns: [

				{ field: 'barcode', caption: 'Barcode', size: '10%' },
                { field: 'item', caption: 'Item', size: '70%' },
				{ field: 'total', caption: 'Total Borrow', size: '10%' },
				{ field: 'totalhours', caption: 'Total Hours', size: '10%' },

			],
			multiSearch: false,
			searches: [
				{ field: 'barcode', caption: 'Barcode', type: 'text' },
			]
		});
	});
</script>
