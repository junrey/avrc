<div id="grid" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/circulation/json.borrowing.item.void.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
			},
			toolbar: {
	            items: [
	                { type: 'button', id: 'void_transaction', caption: 'Void Transaction', icon: 'w2ui-icon-cross', disabled: true }
	            ],
	            onClick: function (target, data) {
	                if (target == 'void_transaction') {
	                    var sel = w2ui['grid'].getSelection();

						w2confirm('Do you want to Void?').yes(function () {
							w2ui['grid'].load( '<?php echo $path; ?>/modules/circulation/json.borrowing.item.void.php?trans_id=' + sel);
					    });
	                }
	            }
	        },
			columns: [
				{ field: 'trans_id', caption: 'Transaction ID', size: '15%' },
				{ field: 'staff', caption: 'Staff', size: '15%' },
				{ field: 'borrower_id', caption: 'Borrower\'s ID', size: '10%' },
				{ field: 'borrower', caption: 'Borrower', size: '20%' },
				{ field: 'borrow_date', caption: 'Borrowed Date', size: '15%' },
				{ field: 'void', caption: 'Void', size: '10%' },

			],
			onSelect: function(event) {
	            w2ui['grid'].toolbar.enable('void_transaction');
	        },
	        onUnselect: function(event) {
	            w2ui['grid'].toolbar.disable('void_transaction');
	        },
			multiSearch: false,
			searches: [
				{ field: 'trans_id', caption: 'Transaction ID', type: 'text' },
				{ field: 'borrower_id', caption: 'Borrower\'s ID', type: 'text' }
			]
		});
	});
</script>
