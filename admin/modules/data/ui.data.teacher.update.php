<?php
set_time_limit(0);

if(isset($_POST["update_db"])) {
	try {
		begin();
		/* ODBC Connection */
		$db_itc_student  = "student";
		$db_itc_teachers = "teachers";
		$conn_odbc_itc   = odbc_connect("Driver={SQL Server};Server=172.22.3.22;Database=$db_itc_student;", "sa", "a");

		$s2 = "SELECT DISTINCT t.tidnumb,t.tlname,t.tfname,t.tmname,t.dept,t.coll FROM $db_itc_teachers.dbo.teachers AS t WHERE t.enable = '1' ORDER BY t.tlname, t.tfname, t.tmname";
		$q2 = odbc_exec($conn_odbc_itc,$s2);

		while($r2 = odbc_fetch_array($q2)) {
			$idnumber = trim($r2['tidnumb']);
			$lname    = mysql_real_escape_string(trim($r2['tlname']));
			$fname    = mysql_real_escape_string(trim($r2['tfname']));
			$mname    = mysql_real_escape_string(trim($r2['tmname']));
			$dept     = mysql_real_escape_string(trim($r2['dept']));
			$coll     = mysql_real_escape_string(trim($r2['coll']));


			$q4 = mysql_query("SELECT idnumber FROM teacher WHERE idnumber = '$idnumber'") or die(mysql_error());
			if(mysql_num_rows($q4) == 0) {
				$s3 = "INSERT INTO teacher(idnumber,lname,fname,mname,dept,coll) VALUES ('$idnumber','$lname','$fname','$mname','$dept','$coll')";
				$q3 = mysql_query($s3) or die(mysql_error());
			} else {
				$s3 = "UPDATE teacher SET lname='$lname',fname='$fname',mname='$mname',dept='$dept',coll='$coll' WHERE idnumber='$idnumber'";
				$q3 = mysql_query($s3) or die(mysql_error());
			}

		}

		mysql_query("UPDATE config SET teacher_db_update = NOW(), teacher_db_update_by = '".getSessionVar('usercode')."'") or die(mysql_error());

		commit();

	} catch(Exception $e) {
		rollback();
		exit();
	}
}

$q5 = mysql_query("SELECT teacher_db_update, teacher_db_update_by FROM config") or die(mysql_error());
$r5 = mysql_fetch_assoc($q5);


?>
<?php include_once("index.header.php"); ?>
<div id="form" style="width:500px;">
    <form name="form_update_db" action="" method="post">
    <div class="w2ui-page page-0">
        <div class="w2ui-field">
            <label>Last Update:</label>
            <div style="padding-top:7px;"><?php echo datetime("M j Y g:i:s a", $r5['teacher_db_update']); ?></div>
        </div>
        <div class="w2ui-field">
            <label>By:</label>
            <div style="padding-top:7px;"><?php echo getUserLogName($r5['teacher_db_update_by']); ?></div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="submit" name="update_db" class="btn btn-green" />Update Teacher Database</button>
    </div>
    </form>
</div>
<script type="text/javascript">
$(function () {
    $('#form').w2form({
        name  : 'form',
        header : '<?php echo $p_menu_name; ?>',
    });
});
</script>
