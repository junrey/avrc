<?php

if(isset($_GET['idnumber'])) {
	$idnumber = $_GET['idnumber'];
} else {
	$idnumber = "";
}

?>
<div id="grid" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({
			name: 'grid',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/data/json.data.borrowing.logs.php?idnumber=<?php echo $idnumber; ?>',
			show: {
				header        : false,
				toolbar       : false,
				footer        : true,
				lineNumbers   : true,
			},
			columns: [

				{ field: 'borrow_date', caption: 'Borrowed Date', size: '18%' },
				{ field: 'barcode', caption: 'Barcode', size: '10%' },
				{ field: 'item', caption: 'Description', size: '10%' },
				{ field: 'staff', caption: 'Served By', size: '10%' },
				{ field: 'return_status', caption: 'Return Status', size: '10%' },
				{ field: 'return_date', caption: 'Date Returned', size: '10%' },
				{ field: 'return_duty', caption: 'Received By', size: '10%' },

			],
		});
		
	});
</script>
