<?php

if(isset($_POST["update_configuration"]) && trim($_POST["client"]) != "" && trim($_POST["system_name"]) != "" && trim($_POST["system_description"]) != "" && trim($_POST["version"]) != "") {
	$client             = escapeString($_POST['client']);
	$system_name        = escapeString($_POST['system_name']);
	$system_description = escapeString($_POST['system_description']);	
	$version            = escapeString($_POST['version']);	

	mysql_query("UPDATE _properties SET client = '$client', system_name = '$system_name', system_description = '$system_description', version = '$version'") or die(mysql_error());
}

$q = mysql_query("SELECT * FROM _properties") or die(mysql_error());
$r = mysql_fetch_assoc($q);

$client             = $r['client'];
$system_name        = $r['system_name'];
$system_description = $r['system_description'];
$version            = $r['version'];

?>
<?php include_once("index.header.php"); ?>
<div id="form" style="width:500px;">
<form name="form_configuration" action="" method="post">
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <div class="w2ui-page page-0">
        <div class="w2ui-field">
            <label>Client:</label>
            <div><input type="text" name="client" value="<?php echo $client; ?>" maxlength="100" required /></div>
        </div>
        <div class="w2ui-field">
            <label>System Name:</label>
            <div><input type="text" name="system_name" maxlength="50" value="<?php echo $system_name; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>System Description:</label>
            <div><input type="text" name="system_description" maxlength="100" value="<?php echo $system_description; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>System Version:</label>
            <div><input type="text" name="version" maxlength="50" value="<?php echo $version; ?>" required /></div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>        
        <button type="submit" class="btn btn-green" name="update_configuration">Save</button>               
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form').w2form({ 
        name  : 'form',
        header : '<?php echo $p_menu_name; ?>',
        fields: [
            { field: 'client', required: true },
            { field: 'system_name', required: true },
            { field: 'system_description', required: true },
            { field: 'version', required: true },
        ]
    });
});
</script>


