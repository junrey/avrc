<?php

if(isset($_GET['code'])) {
	$code = $_GET['code'];
} else {
	$code = "";
}

if(isset($_POST["insert_program"]) && trim($_POST["filename"]) != "" && trim($_POST["menu_name"]) != "") {
	$code          = generateCode('P');
	$keyword       = escapeString($_POST["keyword"]);
	$filename      = escapeString($_POST["filename"]);
	$menu_name     = escapeString($_POST["menu_name"]);
	$width         = escapeString($_POST["width"]);
	$height        = escapeString($_POST["height"]);
	$category_code = $_POST["category_code"];
	$ispopup       = $_POST["ispopup"];
	$active        = $_POST['active'];
	
	$q = mysql_query("SELECT code FROM _program WHERE menu_name = '$menu_name' AND category_code = '$category_code'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
        $code = "";
        $form_submit_status = 2; // duplicate entry
    } else {
		mysql_query("INSERT INTO _program (code, keyword, filename, menu_name, category_code, ispopup, width, height, last_modified_by, last_modified_date, active) VALUES ('$code', '$keyword', '$filename', '$menu_name', '$category_code', '$ispopup', '$width', '$height', '".getSessionVar('usercode')."', NOW(), '$active')") or die(mysql_error());
	}
}

if(isset($_POST["update_program"]) && trim($_POST["filename"]) != "" && trim($_POST["menu_name"]) != "") {
	$code          = escapeString($_POST["code"]);
	$keyword       = escapeString($_POST["keyword"]);
	$filename      = escapeString($_POST["filename"]);
	$menu_name     = escapeString($_POST["menu_name"]);
	$width         = escapeString($_POST["width"]);
	$height        = escapeString($_POST["height"]);
	$category_code = $_POST["category_code"];
	$ispopup       = $_POST["ispopup"];
	$active        = $_POST['active'];
	
	$q = mysql_query("SELECT code FROM _program WHERE menu_name = '$menu_name' AND category_code = '$category_code'");
	if(mysql_num_rows($q) > 0) {
 		mysql_query("UPDATE _program SET keyword = '$keyword', filename = '$filename', category_code = '$category_code', ispopup = '$ispopup', width = '$width', height = '$height', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), active = '$active' WHERE code = '$code'") or die(mysql_error());
    } else {
		mysql_query("UPDATE _program SET menu_name = '$menu_name', keyword = '$keyword', filename = '$filename', category_code = '$category_code', ispopup = '$ispopup', width = '$width', height = '$height', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), active = '$active' WHERE code = '$code'") or die(mysql_error());
	}
}

if($code != "") {
	$q = mysql_query("SELECT * FROM _program WHERE code = '$code'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
		
	$keyword       = $r['keyword'];
	$filename      = $r['filename'];
	$menu_name     = $r['menu_name'];
	$category_code = $r['category_code'];
	$ispopup       = $r['ispopup'];
    $width         = $r['width'];
    $height        = $r['height'];
    $active        = $r['active'];
} else {
	$keyword       =
	$filename      =
	$menu_name     =
	$category_code =
	$ispopup       =
    $width         =
    $height        =
    $active        = "";
}
?>
<?php include_once("index.header.php"); ?>
<div id="form">
<form name="form_program" action="" method="post" >
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <div class="w2ui-page page-0">
        <div class="w2ui-field">
            <label>Keyword:</label>
            <div><input type="text" name="keyword" value="<?php echo $keyword; ?>" maxlength="30" /></div>
        </div>
        <div class="w2ui-field">
            <label>Filename:</label>
            <div><input type="text" name="filename" maxlength="200" value="<?php echo $filename; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Menu Name:</label>
            <div><input type="text" name="menu_name" value="<?php echo $menu_name; ?>" maxlength="30" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Category:</label>
            <div>
                <select name="category_code">
                	<?php $q = mysql_query("SELECT code, name FROM _category WHERE active = '1'") or die(mysql_error()); ?>
				    <?php while($r = mysql_fetch_assoc($q)) { ?>
						<option value="<?php echo $r['code']; ?>" <?php if($category_code == $r['code']) echo "selected"; ?>><?php echo $r['name']; ?></option>
					<?php } ?>
				</select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Popup:</label>
            <div>
                <select name="ispopup">				
					<option value="0" <?php if($ispopup == "0") echo "selected"; ?>>No</option>
                    <option value="1" <?php if($ispopup == "1") echo "selected"; ?>>Yes</option>
				</select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Width:</label>
            <div><input type="text" name="width" maxlength="4" value="<?php echo $width; ?>" onKeyPress="return checkIt(event)" /></div>
        </div>
        <div class="w2ui-field">
            <label>Height:</label>
            <div><input type="text" name="height" maxlength="4" value="<?php echo $height; ?>" onKeyPress="return checkIt(event)" /></div>
        </div>
        <div class="w2ui-field">
            <label>Active:</label>
            <div>
                <select name="active">
                    <option value="1" <?php if($active == "1") echo "selected"; ?>>Yes</option>
                    <option value="0" <?php if($active == "0") echo "selected"; ?>>No</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>
        <?php if($code != "") { ?>
            <button type="submit" class="btn btn-green" name="update_program">Save</button>            
        <?php } else { ?>
            <button type="submit" class="btn btn-green" name="insert_program">Save</button>
        <?php } ?>
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form').w2form({ 
        name  : 'form',        
        fields: [
            { field: 'filename', required: true },
            { field: 'menu_name', required: true },
        ]
    });
});
</script>