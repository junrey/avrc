<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

if(isset($_POST['cmd']) && trim($_POST['cmd']) == "delete-records") {
	$code = escapeString($_POST['selected'][0]);

	$q = mysql_query("SELECT program_code FROM _userprogram WHERE program_code = '$code'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
        $form_submit_status = 2;
    } else {
		mysql_query("DELETE FROM _program WHERE code = '$code'") or die(mysql_error());
	}
}


$limit = "100";
if(isset($_POST['limit'])){
    $limit = escapeString($_POST['limit']);
}

$offset = "0";
if(isset($_POST['offset'])){
    $offset = escapeString($_POST['offset']);
}

$search_field = "";
if(isset($_POST['search'][0]['field'])){
    $search_field = escapeString($_POST['search'][0]['field']);
}

$search_value = "";
if (isset($_POST['search'][0]['value'])){
    $search_value = escapeString($_POST['search'][0]['value']);
}

$q = mysql_query("SELECT * FROM _program WHERE menu_name LIKE '%$search_value%' ORDER BY category_code, menu_name LIMIT $offset, $limit") or die(mysql_error());

$total_records = mysql_num_rows($q);
?>

<?php

echo "{ \"total\": $total_records, \"records\": [";
	$cnt = 0;
	while($r = mysql_fetch_assoc($q)) {
		$cnt++;
		if($cnt > 1) {
			echo ",";
		}
        $ispopup  = ($r['ispopup']) ? "Yes" : "No";
        $active      = ($r['active']) ? "Yes" : "No";

        $q2 = mysql_query("SELECT name FROM _category WHERE code = '$r[category_code]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);

       	echo "{ \"recid\": \"$r[code]\", \"keyword\": \"" . cleanString($r['keyword']) . "\", \"filename\": \"$r[filename]\", \"menu_name\": \"" . cleanString($r['menu_name']) . "\", \"category\": \"" .  cleanString($r2['name']) . "\", \"ispopup\": \"$ispopup\", \"width\": \"$r[width]\", \"height\": \"$r[height]\", \"active\": \"$active\"}";

   }

echo " ] }";

?>
