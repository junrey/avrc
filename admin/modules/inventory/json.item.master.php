<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

if(isset($_POST['cmd']) && trim($_POST['cmd']) == "delete-records") {
	$code = escapeString($_POST['selected'][0]);

    try {
        begin();

        mysql_query("UPDATE inv_item SET deleted = '1' WHERE code = '$code'") or die(mysql_error());

        commit();
    } catch(Exception $e) {
        rollback();
        exit();
    }
}


$limit = "100";
if(isset($_POST['limit'])){
    $limit = escapeString($_POST['limit']);
}

$offset = "0";
if(isset($_POST['offset'])){
    $offset = escapeString($_POST['offset']);
}

$search_field = "";
if(isset($_POST['search'][0]['field'])){
    $search_field = escapeString($_POST['search'][0]['field']);
}

setUTF8();

$search_value = "";
if (isset($_POST['search'][0]['value'])){
    $search_value = escapeString($_POST['search'][0]['value']);
}

$q  = mysql_query("SELECT SQL_CALC_FOUND_ROWS * FROM inv_item WHERE CONCAT(barcode, description) LIKE '%$search_value%' AND deleted = '0' ORDER BY description LIMIT $offset, $limit") or die(mysql_error());
$q2 = mysql_query("SELECT FOUND_ROWS() AS total") or die(mysql_error());
$r2 = mysql_fetch_assoc($q2);

$total_records = $r2['total'];
?>

<?php

echo "{ \"total\": $total_records, \"records\": [";
	$cnt = 0;
	while($r = mysql_fetch_assoc($q)) {
		$cnt++;
		if($cnt > 1) {
			echo ",";
		}
		
        $phaseout = ($r['phaseout']) ? "Yes" : "No";

        $q2 = mysql_query("SELECT name FROM inv_item_category WHERE code = '$r[category_code]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);
        $category = cleanString($r2['name']);

        $q2 = mysql_query("SELECT name FROM inv_item_brand WHERE code = '$r[brand_code]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);
        $brand = cleanString($r2['name']);

        $q2 = mysql_query("SELECT name FROM inv_item_supplier WHERE code = '$r[supplier_code]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);
        $supplier = cleanString($r2['name']);

       	echo "{ \"recid\": \"$r[code]\", \"barcode\": \"$r[barcode]\", \"model_no\": \"$r[model_no]\", \"description\": \"" . cleanString($r['description']) . "\", \"category\": \"" . cleanString($category) . "\", \"brand\": \"" . cleanString($brand) . "\", \"supplier\": \"" . cleanString($supplier) . "\", \"unit_cost\": \"$r[unit_cost]\", \"date_purchased\": \"" . date("m/d/Y", strtotime($r['date_purchased'])) . "\", \"active\": \"$phaseout\"}";
   }

echo " ] }";

?>
