<?php

if(isset($_GET['itemcode'])) {
	$itemcode = $_GET['itemcode'];
} else {
	$itemcode = "";
}

?>
<div id="grid" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({
			name: 'grid',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/inventory/json.item.master.borrowing.logs.php?itemcode=<?php echo $itemcode; ?>',
			show: {
				header        : false,
				toolbar       : false,
				footer        : true,
				lineNumbers   : true,
			},
			columns: [

				{ field: 'borrow_date', caption: 'Borrowed Date', size: '18%' },
				{ field: 'borrower_id', caption: 'ID Number', size: '8%' },
				{ field: 'borrower_name', caption: 'Borrower\'s Name', size: '20%' },
				{ field: 'staff', caption: 'Served By', size: '10%' },
				{ field: 'return_status', caption: 'Return Status', size: '10%' },
				{ field: 'return_date', caption: 'Date Returned', size: '10%' },
				{ field: 'return_duty', caption: 'Received By', size: '10%' },

			],
		});
		
	});
</script>
