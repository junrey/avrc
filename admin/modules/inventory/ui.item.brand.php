<div id="grid" style="width:100%; height:100%;"></div>

<?php 
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]); 
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({ 
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/inventory/json.item.brand.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
				toolbarAdd    : true,
				toolbarDelete : true,
				toolbarEdit   : true
			},      
			columns: [
				{ field: 'name', caption: 'Name', size: '30%' },
				{ field: 'active', caption: 'Active', size: '10%' },
				
			],
			onAdd: function (event) {
                w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_item_brand" class="frame_custom"></iframe>',
                    height: '200',
                });
			},
			onEdit: function (event) {
				var sel = w2ui['grid'].getSelection();
				var recid = sel[0];
               
                 w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_item_brand&code=' + recid + '" class="frame_custom"></iframe>',
                    height: '200'
                });

			},
			onDelete: function (event) {
				event.onComplete = function(){
					 w2alert('Data deleted!');	
				}
			},
			multiSearch: false,
			searches: [
				{ field: 'name', caption: 'Name', type: 'text' }
			]
		});    
	});
</script>
