<?php

if(isset($_GET['code'])) {
	$code = $_GET['code'];
} else {
	$code = "";
}

if(isset($_POST["insert_item_master"]) && trim($_POST["barcode"]) != "" && trim($_POST["description"]) != "" && trim($_POST["unit_cost"]) != "") {
	$code           = generateCode('INV');
	$barcode        = escapeString($_POST['barcode']);
	$model_no       = escapeString($_POST['model_no']);
	$description    = escapeString($_POST['description']);
	$category_code  = $_POST['category_code'];
	$brand_code     = $_POST['brand_code'];
	$supplier_code  = $_POST['supplier_code'];
	$unit_cost      = escapeString($_POST['unit_cost']);

    $date_purchased = date("Y-m-d", strtotime($_POST['date_purchased']));
	$phaseout       = $_POST['phaseout'];

	$q = mysql_query("SELECT barcode FROM inv_item WHERE barcode = '$barcode'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
        $code = "";
        $form_submit_status = 2; // duplicate entry
	} else {
        try {
			begin();

            mysql_query("INSERT INTO inv_item(code, barcode, model_no, description, category_code, brand_code, supplier_code, unit_cost, date_purchased, last_modified_by, last_modified_date, phaseout) VALUES ('$code', '$barcode', '$model_no', '$description','$category_code', '$brand_code', '$supplier_code', '$unit_cost', '$date_purchased', '".getSessionVar('usercode')."', NOW(), '$phaseout')") or die(mysql_error());

            commit();
            $form_submit_status = 1;
		} catch(Exception $e) {
			rollback();
			exit();
		}
    }
}

if(isset($_POST["update_item_master"])  && trim($_POST["barcode"]) != "" && trim($_POST["description"]) != "" && trim($_POST["unit_cost"]) != "") {
	$code           = escapeString($_POST["code"]);
	$barcode        = escapeString($_POST['barcode']);
	$model_no       = escapeString($_POST['model_no']);
	$description    = escapeString($_POST['description']);
	$category_code  = $_POST['category_code'];
	$brand_code     = $_POST['brand_code'];
	$supplier_code  = $_POST['supplier_code'];
	$unit_cost      = escapeString($_POST['unit_cost']);
    $date_purchased = date("Y-m-d", strtotime($_POST['date_purchased']));
	$phaseout       = $_POST['phaseout'];

    try {
        begin();

        $q = mysql_query("SELECT barcode FROM inv_item WHERE barcode = '$barcode'") or die(mysql_error());

        if(mysql_num_rows($q) > 0) {
            mysql_query("UPDATE inv_item SET model_no = '$model_no', description = '$description', category_code = '$category_code', brand_code = '$brand_code', supplier_code = '$supplier_code', unit_cost = '$unit_cost', date_purchased = '$date_purchased', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), phaseout = '$phaseout' WHERE code = '$code'") or die(mysql_error());
        } else {
            mysql_query("UPDATE inv_item SET barcode = '$barcode', model_no = '$model_no', description = '$description', category_code = '$category_code', brand_code = '$brand_code', supplier_code = '$supplier_code', unit_cost = '$unit_cost', date_purchased = '$date_purchased', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), phaseout = '$phaseout' WHERE code = '$code'") or die(mysql_error());
        }

        commit();
        $form_submit_status = 1;
    } catch(Exception $e) {
        rollback();
        exit();
    }
}

if($code != "") {
	$q = mysql_query("SELECT * FROM inv_item WHERE code = '$code'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);

	$barcode        = $r['barcode'];
    $model_no       = $r['model_no'];
    $description    = $r['description'];
    $category_code  = $r['category_code'];
    $brand_code     = $r['brand_code'];
    $supplier_code  = $r['supplier_code'];
    $unit_cost      = $r['unit_cost'];
    $date_purchased = $r['date_purchased'];
    $phaseout       = $r['phaseout'];
} else {
	$barcode        =
    $model_no       =
    $description    =
    $category_code  =
    $brand_code     =
    $supplier_code  =
    $unit_cost      =
    $date_purchased =
    $phaseout       = "";
}


?>

<?php include_once("index.header.php"); ?>
<div id="form">
<form name="form_entry" action="" method="post" >
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <div class="w2ui-page page-0">
        <div class="w2ui-field">
            <label>Barcode / Serial:</label>
            <div><input type="text" name="barcode" maxlength="30" value="<?php echo $barcode; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Model Number:</label>
            <div><input type="text" name="model_no" maxlength="30" value="<?php echo $model_no; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Description:</label>
            <div><input type="text" name="description" maxlength="500" value="<?php echo $description; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Category:</label>
            <div>
                <select name="category_code">
                    <option value="NONE">NONE</option>
                    <?php $q = mysql_query("SELECT code, name FROM inv_item_category WHERE active = '1' ORDER BY name") or die(mysql_error()); ?>
                    <?php while($r = mysql_fetch_assoc($q)) { ?>
                            <option value="<?php echo $r['code']; ?>" <?php if($category_code == $r['code']) echo "selected"; ?>><?php echo $r['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Brand:</label>
            <div>
                <select name="brand_code">
                    <option value="NONE">NONE</option>
                    <?php $q = mysql_query("SELECT code, name FROM inv_item_brand WHERE active = '1' ORDER BY name") or die(mysql_error()); ?>
                    <?php while($r = mysql_fetch_assoc($q)) { ?>
                            <option value="<?php echo $r['code']; ?>" <?php if($brand_code == $r['code']) echo "selected"; ?>><?php echo $r['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Supplier:</label>
            <div>
                <select name="supplier_code">
                    <option value="NONE">NONE</option>
                    <?php $q = mysql_query("SELECT code, name FROM inv_item_supplier WHERE active = '1' ORDER BY name") or die(mysql_error()); ?>
                    <?php while($r = mysql_fetch_assoc($q)) { ?>
                            <option value="<?php echo $r['code']; ?>" <?php if($supplier_code == $r['code']) echo "selected"; ?>><?php echo $r['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Unit Cost:</label>
            <div><input type="text" name="unit_cost" maxlength="9" value="<?php echo $model_no; ?>" onKeyPress="return checkIt(event)" /></div>
        </div>
        <div class="w2ui-field">
            <label>Date Purchased:</label>
            <div><input type="text" name="date_purchased" id="date_purchased"></div>
        </div>
        <div class="w2ui-field">
            <label>Phaseout:</label>
            <div>
                <select name="phaseout">
                    <option value="0" <?php if($phaseout == "0") echo "selected"; ?>>No</option>
                    <option value="1" <?php if($phaseout == "1") echo "selected"; ?>>Yes</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>
        <?php if($code != "") { ?>
            <button type="submit" class="btn btn-green" name="update_item_master">Save</button>
        <?php } else { ?>
            <button type="submit" class="btn btn-green" name="insert_item_master">Save</button>
        <?php } ?>
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {

    $('#form').w2form({
        name  : 'form',
        fields: [
            { field: 'barcode', required: true },
            { field: 'description', required: true },
            { field: 'unit_cost', required: true },
            { field: 'date_purchased', type: 'date' },
        ]
    });

	<?php if($date_purchased != "") { ?>
    	w2ui['form'].record['date_purchased'] = '<?php echo date("m/d/Y", strtotime($date_purchased)); ?>';
	<?php } ?>

});
</script>
