<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

$date_from = date('Y') . "-" . date('m') . "-" . date('d');
$date_to   = date('Y') . "-" . date('m') . "-" . date('d');

if(isset($_GET['date_from']) && trim($_GET['date_from']) != "") {
	$date_from = date("Y-m-d", strtotime(trim($_GET['date_from'])));
}

if(isset($_GET['date_to']) && trim($_GET['date_to']) != "") {
	$date_to = date("Y-m-d", strtotime(trim($_GET['date_to'])));
}


header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=borrowing_borrowed_item_report" . Date("mdY") . ".xls");

setUTF8();
$q  = mysql_query("SELECT a.* FROM borrow_item AS a INNER JOIN borrow_header AS b ON a.borrow_header = b.code WHERE a.borrow_header <> '' AND a.void = '0' AND (DATE(a.borrow_date) BETWEEN '$date_from' AND '$date_to') ORDER BY a.borrow_date DESC") or die(mysql_error());

?>
<table border="1">
    <tr>
        <th>Borrowed Date</th>
        <th>ID Number</th>
        <th>Borrower's Name</th>
        <th>Teacher</th>
        <th>Subject</th>
        <th>Remarks</th>
        <th>Barcode</th>
        <th>Item</th>
        <th>Venue</th>
        <th>Served By</th>
        <th>Return Status</th>
        <th>Date Returned</th>
        <th>Received By</th>
    </tr>
    <?php while($r = mysql_fetch_assoc($q)) {
    		$borrower = "";
    		$q2 = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$r[borrower]'");
    		if(mysql_num_rows($q2) > 0) {
    			$r2 = mysql_fetch_assoc($q2);
    			$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

    		} else {
    			$q2 = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$r[borrower]'");
    			if(mysql_num_rows($q2) > 0) {
    				$r2 = mysql_fetch_assoc($q2);
    				$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

    			} else {
    				$borrower = $r['borrower'];
    			}

    		}

            $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
            $r2 = mysql_fetch_assoc($q2);
			
			$return_status = "Yes";
			if($r['item_return'] == "0") {
				$return_status = "No";
			}
	
			if($r['a_return_date'] == "0000-00-00 00:00:00") {
				$return_date = "";	
			} else {
				$return_date = datetime("m/j/y h:i:s a", $r['a_return_date']);
			}
    ?>
    <tr>
        <td><?php echo datetime("m/j/y h:i:s a", $r['borrow_date']); ?></td>
        <td><?php echo "&nbsp;" . $r['borrower']; ?></td>
        <td><?php echo htmlentities($borrower, ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['teacher'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['subject'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['remarks'], ENT_QUOTES); ?></td>
        <td><?php echo "&nbsp;" . $r['barcode']; ?></td>
        <td><?php echo htmlentities($r2['description'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['venue'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities(getUserLogName($r['usercode']), ENT_QUOTES); ?></td>
        <td><?php echo $return_status; ?></td>
        <td><?php echo $return_date; ?></td>
        <td><?php echo htmlentities(getUserLogName($r['usercode']), ENT_QUOTES); ?></td>
    </tr>
    <?php } ?>
</table>
