<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=item_usage_report_" . Date("mdY") . ".xls");

setUTF8();

$q  = mysql_query("SELECT COUNT(barcode) AS total, barcode, SEC_TO_TIME(SUM(TIME_TO_SEC(timediff(a_return_date, borrow_date)))) AS totalhours FROM borrow_item WHERE borrow_header <> '' AND void = '0' AND item_return = '1' GROUP BY barcode ORDER BY total DESC") or die(mysql_error());

?>
<table border="1">
    <tr>
        <th>Barcode</th>
        <th>Item</th>
        <th>Total Borrow</th>
        <th>Total Hours</th>
    </tr>
    <?php while($r = mysql_fetch_assoc($q)) {
        $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);
    ?>
    <tr>
        <td><?php echo "&nbsp;" . $r['barcode']; ?></td>
        <td><?php echo htmlentities($r2['description'], ENT_QUOTES); ?></td>
        <td><?php echo $r['total']; ?></td>
        <td><?php echo $r['totalhours']; ?></td>
    </tr>
    <?php } ?>
</table>
