<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=borrowing_report_teacher_" . Date("mdY") . ".xls");

$coll = "ALL";
if(isset($_GET['coll'])){
    $coll = escapeString($_GET['coll']);
}

$sem  = getSessionVar('sem');
if(isset($_GET['sem'])){
    $sem = escapeString($_GET['sem']);
}

$term = getSessionVar('term');
if(isset($_GET['term'])){
    $term = escapeString($_GET['term']);
}

$ay   = getSessionVar('ay');
if(isset($_GET['ay'])){
    $ay = escapeString($_GET['ay']);
}

setUTF8();

if($coll == "ALL") {
	$q = mysql_query("SELECT a.*, b.ay, CASE b.sem WHEN '1' THEN '1st' WHEN '2' THEN '2nd' WHEN '3' THEN 'Summer' END AS sem FROM borrow_item AS a INNER JOIN borrow_header AS b ON a.borrow_header = b.code WHERE a.borrow_header <> '' AND a.void = '0' AND b.sem = '$sem' AND b.ay = '$ay' AND b.isstudent = '1' ORDER BY a.borrow_date DESC") or die(mysql_error());
} else {
	$q = mysql_query("SELECT a.*, b.ay, CASE b.sem WHEN '1' THEN '1st' WHEN '2' THEN '2nd' WHEN '3' THEN 'Summer' END AS sem FROM borrow_item AS a INNER JOIN borrow_header AS b ON a.borrow_header = b.code WHERE a.borrow_header <> '' AND a.void = '0' AND b.sem = '$sem' AND b.ay = '$ay' AND b.isstudent = '1' AND b.coll = '$coll' ORDER BY a.borrow_date DESC") or die(mysql_error());
}

?>
<table border="1">
    <tr>
        <th>Borrow Date</th>
        <th>ID Number</th>
        <th>Borrower Name</th>
        <th>Course</th>
        <th>Major</th>
        <th>Year</th>
        <th>Section</th>
        <th>College</th>
        <th>Teacher</th>
        <th>Subject</th>
        <th>Remarks</th>
        <th>Barcode</th>
        <th>Item</th>
        <th>Venue</th>
        <th>Borrow Duty</th>
        <th>Return Status</th>
        <th>Return Date</th>
        <th>Return Duty</th>
        <th>Semester</th>
        <th>Academic Year</th>
    </tr>
    <?php while($r = mysql_fetch_assoc($q)) {
		
		$q3 = mysql_query("SELECT c.lname, c.fname, c.course, c.major, c.year, c.section, c.coll, CASE c.coll WHEN 'AS' THEN 'CAS' WHEN 'CO' THEN 'CBA' WHEN 'EN' THEN 'ENG' WHEN 'BE' THEN 'EDU' WHEN 'BN' THEN 'BSN' WHEN 'LAW' THEN 'LAW' ELSE 'OTHR' END AS college FROM student AS c WHERE c.idnumber = '$r[borrower]' LIMIT 1") or die(mysql_error());
		$r3 = mysql_fetch_assoc($q3);

		
        $borrower = strtoupper($r3['lname']) . ", " .strtoupper($r3['fname']);

        $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);

        $status    = "Yes";
        $status_bg = "";

        if($r['item_return'] == "0") {
            $status    = "No";
        }

    ?>
    <tr>
        <td><?php echo datetime("m/j/y h:i:s a", $r['borrow_date']); ?></td>
        <td><?php echo "&nbsp;" .$r['borrower']; ?></td>
        <td><?php echo htmlentities($borrower, ENT_QUOTES); ?></td>
        <td><?php echo $r3['course']; ?></td>
        <td><?php echo $r3['major']; ?></td>
        <td><?php echo $r3['year']; ?></td>
        <td><?php echo $r3['section']; ?></td>
        <td><?php echo $r3['college']; ?></td>
        <td><?php echo htmlentities($r['teacher'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['subject'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['remarks'], ENT_QUOTES); ?></td>
        <td><?php echo "&nbsp;" . $r['barcode']; ?></td>
        <td><?php echo htmlentities($r2['description'], ENT_QUOTES); ?></td>
        <td><?php echo $r['venue']; ?></td>
        <td><?php echo htmlentities(getUserLogName($r['usercode']), ENT_QUOTES); ?></td>
        <td><?php echo $status; ?></td>
        <?php
			if($r['a_return_date'] == "0000-00-00 00:00:00") {
				$return_date = "";	
			} else {
				$return_date = datetime("m/j/y h:i:s a", $r['a_return_date']);
			}
		?>
        <td><?php echo $return_date; ?></td>
        <td><?php echo htmlentities(getUserLogName($r['rusercode']), ENT_QUOTES) ?></td>
        <td><?php echo $r['sem']; ?></td>
        <td><?php echo $r['ay']; ?></td>
    </tr>
    <?php } ?>
</table>
