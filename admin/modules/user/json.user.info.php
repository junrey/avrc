<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

if(isset($_POST['cmd']) && trim($_POST['cmd']) == "delete-records") {
	$usercode = escapeString($_POST['selected'][0]);

    try {
        begin();

        mysql_query("UPDATE _user SET deleted = '1' WHERE usercode = '$usercode'") or die(mysql_error());

        commit();
    } catch(Exception $e) {
        rollback();
        exit();
    }
}


$limit = "100";
if(isset($_POST['limit'])){
    $limit = escapeString($_POST['limit']);
}

$offset = "0";
if(isset($_POST['offset'])){
    $offset = escapeString($_POST['offset']);
}

$search_field = "";
if(isset($_POST['search'][0]['field'])){
    $search_field = escapeString($_POST['search'][0]['field']);
}

$search_value = "";
if (isset($_POST['search'][0]['value'])){
    $search_value = escapeString($_POST['search'][0]['value']);
}

setUTF8();

$q  = mysql_query("SELECT SQL_CALC_FOUND_ROWS * FROM _user WHERE CONCAT(idnumber,lname) LIKE '%$search_value%' AND deleted = '0' ORDER BY lname, fname, mname LIMIT $offset, $limit") or die(mysql_error());
$q2 = mysql_query("SELECT FOUND_ROWS() AS total") or die(mysql_error());
$r2 = mysql_fetch_assoc($q2);

$total_records = $r2['total'];
?>

<?php

echo "{ \"total\": $total_records, \"records\": [";
	$cnt = 0;
	while($r = mysql_fetch_assoc($q)) {
		$cnt++;
		if($cnt > 1) {
			echo ",";
		}
		
        $active      = ($r['active']) ? "Yes" : "No";

       	echo "{ \"recid\": \"$r[usercode]\", \"idnumber\": \"" . cleanString($r['idnumber']) . "\", \"username\": \"" . cleanString($r['username']) . "\", \"lname\": \"" . cleanString($r['lname']) . "\", \"fname\": \"" . cleanString($r['fname']) . "\", \"mname\": \"" . cleanString($r['mname']) . "\", \"active\": \"$active\"}";
   }

echo " ] }";

?>
