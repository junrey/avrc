<?php

if(isset($_GET['usercode'])) {
	$usercode = $_GET['usercode'];
} else {
	$usercode = "";
}

$q = mysql_query("SELECT username, lname, fname FROM _user WHERE usercode = '$usercode'") or die(mysql_error());
$r = mysql_fetch_assoc($q);

$username = $r['username'];
$lname    = $r['lname'];
$fname    = $r['fname'];

?>
<div style="position: relative; height: 100%;">
    <div id="grid1" style="position: absolute; left: 0px; width: 49.9%; height: 100%;"></div>
    <div id="grid2" style="position: absolute; right: 0px; width: 49.9%; height: 100%"></div>
</div>
<?php 
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]); 
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid1').w2grid({ 
			name: 'grid1',
			header: 'Available Rights',
			url: '<?php echo $path; ?>/modules/user/json.user.program.php?usercode=<?php echo $usercode; ?>',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
                selectColumn  : true,
				lineNumbers   : true,
				toolbarSearch : false,
                toolbarReload : false,
                toolbarColumns: false
			},
            toolbar: {
                items: [
                    { type: 'button', id: 'add_access', caption: 'Add Access', icon: 'w2ui-icon-plus', disabled: true }
                ],
                onClick: function (target, data) {
                    if (target == 'add_access') {				
                        var sel = w2ui['grid1'].getSelection();
                        w2ui['grid2'].load( '<?php echo $path; ?>/modules/user/json.user.program.assign.php?usercode=<?php echo $usercode; ?>&addaccess=' + sel);
                        w2ui['grid1'].reload();
                    }
                }
            },
			columns: [
                { field: 'category', caption: 'Category', size: '30%' },
                { field: 'program', caption: 'Program', size: '70%' },			
			],
            onSelect: function(event) {
                w2ui['grid1'].toolbar.enable('add_access');
            },
            onUnselect: function(event) {
                w2ui['grid1'].toolbar.disable('add_access');
            }, 
		});
        
        $('#grid2').w2grid({ 
			name: 'grid2',
			header: '<?php echo "[" . $username . "] Access Rights"; ?>',
			url: '<?php echo $path; ?>/modules/user/json.user.program.assign.php?usercode=<?php echo $usercode; ?>',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
                selectColumn  : true,
				lineNumbers   : true,
				toolbarSearch : false,
                toolbarReload : false,
                toolbarColumns: false
			},
            toolbar: {
                items: [
                    { type: 'button', id: 'remove_access', caption: 'Remove Access', icon: 'w2ui-icon-cross', disabled: true }
                ],
                onClick: function (target, data) {
                    if (target == 'remove_access') {				
                        var sel = w2ui['grid2'].getSelection();
                        w2ui['grid1'].load( '<?php echo $path; ?>/modules/user/json.user.program.php?usercode=<?php echo $usercode; ?>&removeaccess=' + sel);
                        w2ui['grid2'].reload();
                    }
                }
            },
			columns: [
                { field: 'category', caption: 'Category', size: '30%' },
                { field: 'program', caption: 'Program', size: '70%' },			
			],
            onSelect: function(event) {
                w2ui['grid2'].toolbar.enable('remove_access');
            },
            onUnselect: function(event) {
                w2ui['grid2'].toolbar.disable('remove_access');
            }, 
		});
        
       
	});
</script>


