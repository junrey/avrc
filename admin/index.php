<?php session_start();
    require_once("../db/db.connection.php");
    require_once("../inc/inc.functions.php");

    if(!isset($_SESSION[getSystemName()]['usercode'])) {
        header ("location: ../login.php");
    }
?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo getClient(); ?> | <?php echo getSystemName(); ?></title>
<!-- include header -->
<?php include_once("index.header.php"); ?>
</head>
<body>

<div id="layout" style="position: absolute; width: 100%; height: 100%;"></div>

<script>
$(function () {
    var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';

    $('#layout').w2layout({
        name: 'layout',
        panels: [
            { type: 'top', size: 40, style: pstyle, content: 'top' },
            { type: 'left', size: 250, style: pstyle, resizable: true, minSize: 120 },
            { type: 'main', style: pstyle}
        ]
    });

    w2ui['layout'].content('top',  $().w2toolbar({
        name: 'toolbar',
        style: 'background-image: linear-gradient(#dae6f3,#c2d5ed);',
        items: [
            { type: 'html', html: '<div style="color:#444; font-weight: bold; margin-left:10px;"><?php echo getSystemName(); ?></div>'},
            { type: 'spacer' },
            { type: 'html', html: 'Term: <b><?php if(getSessionVar("term") == "1") echo "Prelim"; else if(getSessionVar("term") == "2") echo "Midterm"; else echo "Endterm"; ?></b>' },
            { type: 'break'},
            { type: 'html', html: 'Sem: <b><?php if(getSessionVar("sem") == "1") echo "1st"; else if(getSessionVar("sem") == "2") echo "2nd"; else echo "Summer"; ?></b>' },
            { type: 'break' },
            { type: 'html', html: 'AY: <b><?php echo getSessionVar("ay"); ?></b>' },
            { type: 'break'},
            { type: 'html', html: '<button onclick="userProfile();" class="btn"><?php echo getSessionVar("name"); ?></button>' },
            { type: 'html', html: '<button onclick="document.location.href=\'../logout.php\'" class="btn btn-red">Logout</button>' },
        ]
    }));

    // then define the sidebar
    w2ui['layout'].content('left', $().w2sidebar({
        name: 'sidebar',
        nodes : [
            <?php
                if(getSessionVar('usercode') == getRootCode()) {
                    $q = mysql_query("SELECT code, name FROM _category WHERE parent_code = '0' AND popup_menu = '0' AND active = '1' ORDER BY name") or die(mysql_error());
                } else {
                    $q = mysql_query("SELECT DISTINCT a.code, a.name FROM _category a INNER JOIN _program b ON a.code = b.category_code INNER JOIN _userprogram c ON b.code = c.program_code WHERE a.parent_code = '0' AND a.popup_menu = '0' AND a.active = '1' AND c.usercode = '".getSessionVar('usercode')."' ORDER BY a.name") or die(mysql_error());
                }

                while($r = mysql_fetch_assoc($q)) {
                    echo "{ id: '$r[code]', text: '$r[name]', img: 'icon-folder', expanded: false,";
                    echo "nodes: [";
                    displayMenu($r['code']);
                    echo "]";
                    echo "},";
                }
            ?>
        ],
        onClick: function (event) {
            var menu_id = event.target;
            if(menu_id.toUpperCase().charAt(0) != "C") { // apply this function to menu item ony
               // $().w2destroy('grid');
               // $().w2destroy('form');
                w2ui['layout'].content('main', '<iframe src="router.php?p=' + event.target + '" class="frame_custom"></iframe>');

            }
        }
    }));
});

function userProfile() {
    w2popup.open({
        title  : 'User Profile',
        style  : 'padding:5px; overflow: hidden',
        body   : '<iframe src="router.php?p=update_user_profile" class="frame_custom"></iframe>',
        height : '340'
    });
}
</script>

</body>
</html>
