function getPage(id, page, loader, loader_type) {
	if (typeof loader_type == 'undefined') loader_type = 1;
	
	var xmlhttp=false; //Clear our fetching variable
   	try  {
       xmlhttp = new ActiveXObject('Msxml2.XMLHTTP'); //Try the first kind of active x object
	} catch (e) {    
		try {
                xmlhttp = new
                ActiveXObject('Microsoft.XMLHTTP'); //Try the second kind of active x object
        } catch (E) {
                xmlhttp = false;
        }
   	}
   	
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest(); //If we were able to get a working active x object, start an XMLHttpRequest
   	}

	xmlhttp.open('GET', page, true); //Open the file through GET, and add the page we want to retrieve as a GET variable **
	//setLoaderType(loader, loader_type, 1);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4) { //Check if it is ready to recieve data
			var content = xmlhttp.responseText; //The content data which has been retrieved ***
			//setLoaderType(loader, loader_type, 0);
			if( content ) { //Make sure there is something in the content variable
			  document.getElementById(id).innerHTML = content; //Change the inner content of your div to the newly retrieved content ****
			}
		}
	}
	
	xmlhttp.send(null) //Nullify the XMLHttpRequest
	
}

function getPagePrint(id, page, loader, loader_type) {
	if (typeof loader_type == 'undefined') loader_type = 1;
	
	var xmlhttp=false; //Clear our fetching variable
   	try  {
       xmlhttp = new ActiveXObject('Msxml2.XMLHTTP'); //Try the first kind of active x object
	} catch (e) {    
		try {
                xmlhttp = new
                ActiveXObject('Microsoft.XMLHTTP'); //Try the second kind of active x object
        } catch (E) {
                xmlhttp = false;
        }
   	}
   	
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest(); //If we were able to get a working active x object, start an XMLHttpRequest
   	}

	xmlhttp.open('GET', page, true); //Open the file through GET, and add the page we want to retrieve as a GET variable **
	//setLoaderType(loader, loader_type, 1);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4) { //Check if it is ready to recieve data
			var content = xmlhttp.responseText; //The content data which has been retrieved ***
			//setLoaderType(loader, loader_type, 0);
			if( content ) { //Make sure there is something in the content variable
			  document.getElementById(id).innerHTML = content; //Change the inner content of your div to the newly retrieved content ****
			  window.print();
			}
		}
	}
	
	xmlhttp.send(null) //Nullify the XMLHttpRequest
	
}
function getPageNoLoader(id, page) {
	var xmlhttp=false; //Clear our fetching variable
   	try  {
       xmlhttp = new ActiveXObject('Msxml2.XMLHTTP'); //Try the first kind of active x object
	} catch (e) {    
		try {
                xmlhttp = new
                ActiveXObject('Microsoft.XMLHTTP'); //Try the second kind of active x object
        } catch (E) {
                xmlhttp = false;
        }
   	}
   	
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest(); //If we were able to get a working active x object, start an XMLHttpRequest
   	}

	xmlhttp.open('GET', page, true); //Open the file through GET, and add the page we want to retrieve as a GET variable **
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4) { //Check if it is ready to recieve data
			var content = xmlhttp.responseText; //The content data which has been retrieved ***
			  if( content ) { //Make sure there is something in the content variable
			  	document.getElementById(id).innerHTML = content; //Change the inner content of your div to the newly retrieved content ****
			  }
			}
		}

	xmlhttp.send(null) //Nullify the XMLHttpRequest
	return;
}

function getPageNoReturn(page) {
	var xmlhttp=false; //Clear our fetching variable
   	try  {
       xmlhttp = new ActiveXObject('Msxml2.XMLHTTP'); //Try the first kind of active x object
	} catch (e) {    
		try {
                xmlhttp = new
                ActiveXObject('Microsoft.XMLHTTP'); //Try the second kind of active x object
        } catch (E) {
                xmlhttp = false;
        }
   	}
   	
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest(); //If we were able to get a working active x object, start an XMLHttpRequest
   	}

	xmlhttp.open('GET', page, true); //Open the file through GET, and add the page we want to retrieve as a GET variable **
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4) { //Check if it is ready to recieve data
			var content = xmlhttp.responseText; //The content data which has been retrieved ***
			  if( content ) { //Make sure there is something in the content variable
			  	//document.getElementById(id).innerHTML = content; //Change the inner content of your div to the newly retrieved content ****
			  }
			}
		}

	xmlhttp.send(null) //Nullify the XMLHttpRequest
	return;
}

function getPageReturnValue(id,page) {
	var xmlhttp=false; //Clear our fetching variable
   	try  {
       xmlhttp = new ActiveXObject('Msxml2.XMLHTTP'); //Try the first kind of active x object
	} catch (e) {    
		try {
                xmlhttp = new
                ActiveXObject('Microsoft.XMLHTTP'); //Try the second kind of active x object
        } catch (E) {
                xmlhttp = false;
        }
   	}
   	
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest(); //If we were able to get a working active x object, start an XMLHttpRequest
   	}

	xmlhttp.open('GET', page, true); //Open the file through GET, and add the page we want to retrieve as a GET variable **
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4) { //Check if it is ready to recieve data
			var content = xmlhttp.responseText; //The content data which has been retrieved ***
			  if( content ) { //Make sure there is something in the content variable
			  	document.getElementById(id).value = content; //Change the inner content of your div to the newly retrieved content ****
			  }
			}
		}

	xmlhttp.send(null) //Nullify the XMLHttpRequest
	return;
}

function getPostPage(id, page, loader, param, loader_type) {
	if (typeof loader_type == 'undefined') loader_type = 1;
	
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			//setLoaderType(loader, loader_type, 0);
			var content = xmlhttp.responseText;
			if(content) {
				document.getElementById(id).innerHTML = content;
			}
		}
	}
	xmlhttp.open("POST",page,true);
	//setLoaderType(loader, loader_type, 1);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(param);
}

function getPostPagePrint(id,page, loader, param, loader_type) {
	if (typeof loader_type == 'undefined') loader_type = 1;
	
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			//setLoaderType(loader, loader_type, 0);
			var content = xmlhttp.responseText;
			if(content) {
				document.getElementById(id).innerHTML = content;
				window.print();
			}
		}
	}
	xmlhttp.open("POST",page,true);
	//setLoaderType(loader, loader_type, 1);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(param);
}

function setLoaderType(loader_id, type, status) {
	if(type == 1) {
		if(status == 1) document.getElementById(loader_id).style.display = "block";
		else document.getElementById(loader_id).style.display = "none";
	} else if(type == 2) {
		if(status == 1) $('#' + loader_id).dialog('open');
		else $('#' + loader_id).dialog('close');
	}
	
}
