function createWindow(code, url, title, w, h) {
	w = ((w != null) ? w : 500);
	h = ((h != null) ? h : 300);

	$("<div id='D" + code + "' title='" + title + "'><iframe id='F" + code +"' src='" + url + "'></iframe></div>")
    .appendTo('body')
    .dialog({
        modal: true,
        width: w,
        height: h,
		position: 'center', 
		resizable: false, 
		draggable: false,
   		close: function(event, ui){ 
			$(this).remove();
		}
    });
}

function createDialog(id, title, w, h) {
	w = ((w != null) ? w : 500);
	h = ((h != null) ? h : 300);
	
	$('.' + id).dialog({
		title: title,
		autoOpen: true, 
		modal: true,
		width: w, 
		height: h, 
		position: 'center', 
		resizable: false, 
		draggable: false
	
	}); 
}


function checkIt(evt) {
	evt = (evt) ? evt : window.event
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			if(charCode != 46){
				status = "This field accepts numbers only."
				return false
			}
		
	} 
	status = ""
	return true
}
// usage: onKeyPress="return checkIt(event)"
