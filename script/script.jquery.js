$(function(){

$('table thead').addClass("ui-widget-header");
$('table tbody').addClass("ui-widget-content");
	   
//$('input, textarea, select').addClass('ui-corner-all');
$('button, input[type="button"], input[type="submit"],input[type="reset"]').button();

$('.delete').click(function() { return confirm('Do you want to delete?'); });

$('.datenow').datepicker({ dateFormat: 'yy-mm-dd' });
$('.daterange').datepicker({ changeMonth:true, changeYear:true, dateFormat:'yy-mm-dd' });

$('.pagination li').button();

$(".tab").tabs({
	spinner: "Loading...",
	ajaxOptions: {
		error: function( xhr, status, index, anchor ) {
			$( anchor.hash ).html("Couldn't load this tab. Please try again." );
		}
	}
});

// ajax loading
//$("#loading_status").dialog({ modal:true, width:200, height:100, autoOpen:false, draggable:false, resizable:false});
//$("#loading_status").siblings(".ui-dialog-titlebar").hide();  
jQuery(document).ajaxStart(function() { $('#loading_status').show(); }).ajaxStop(function() { $('#loading_status').hide(); });
jQuery.ajaxSetup({async:false});

$(document).tooltip();

});
