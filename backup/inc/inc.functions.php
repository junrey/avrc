<?php 

function getClient() {
	$q = mysql_query("SELECT client FROM _properties") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
    $client = $r['client'];
	
	return $client;
}
function getSystemName() {
	$q = mysql_query("SELECT system_name FROM _properties") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
    $system_name = $r['system_name'];

	return $system_name;
}

function getSystemDescription() {
	$q = mysql_query("SELECT system_description FROM _properties") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
    $system_description = $r['system_description'];

	return $system_description;
}

function getSessionVar($var) {
	return $_SESSION[getSystemName()][$var];
}
function getRootCode() {
	return "dbadmin";
}
function getRootUsername() {
	return "dbadmin";
}
function escapeString($s) {
	return mysql_real_escape_string(trim($s));
}
function cleanString($s) {
	$s = preg_replace('/[^A-Za-z0-9\. -]/', '', $s);
	$s = htmlentities($s, ENT_QUOTES);

	return setUTF8String($s);
}
function setUTF8String($s) {
	return mb_convert_encoding(trim($s), 'utf-8');
}
function generateCode($prx) {
    return strtoupper(uniqid($prx));
}
function getUserLogName($usercode) {
	$name = "";
	$q = mysql_query("SELECT fname, lname FROM _user WHERE usercode = '$usercode'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
    $name = strtoupper($r['fname'][0] . ". " . $r['lname']);

	return $name;
}

function checkAllowEntry($usercode, $program_code) {
	$allow_entry = 1;
	$q = mysql_query("SELECT allow_entry FROM _userprogram WHERE usercode = '$usercode' AND program_code = '$program_code'")  or die(mysql_error());
	$r = mysql_fetch_assoc($q);
    $allow_entry = $r['allow_entry'];
	
	return $allow_entry;
}

//---------------------- all about money
function formatMoney($number) {
	return number_format($number,2,".",",");
}

function datetime($syntax,$datetime) {
	$year = substr($datetime,0,4);
	$month = substr($datetime,5,2);
	$day = substr($datetime,8,2);
	$hour = substr($datetime,11,2);
	$min = substr($datetime,14,2);
	$sec = substr($datetime,17,2);
	
	return date($syntax,mktime($hour,$min,$sec,$month,$day,$year));
}

function displayError01() {
	return "<script>alert('Cant delete! assigned to a user');</script>";
}

function displayError02() {
	return "<script>alert('Duplicate Entry!');</script>";
}

/* global variable */
$error_display_no_username = "NO USERNAME FOUND! PLEASE REFRESH YOUR BROWSER.";

//-------------------------

function displayMenu($parent_code) {
	$q = mysql_query("SELECT code, name FROM _category WHERE parent_code = '$parent_code' AND popup_menu = '0' AND active = '1'");
	while($r = mysql_fetch_assoc($q)) {
        echo "{ id: '$r[code]', text: '$r[name]', img: 'icon-folder', expanded: false,";
                    
        echo "nodes: [";

        displayMenu($r['code']);
        
        echo "]";

        echo "},";

	}
	
	if(getSessionVar('usercode') == getRootCode()) {
		$q = mysql_query("SELECT code, menu_name FROM _program WHERE ispopup = '0' AND category_code = '$parent_code' ORDER BY menu_name");
	} else {
		$q = mysql_query("SELECT DISTINCT a.code, a.menu_name FROM _program a INNER JOIN _userprogram b ON a.code = b.program_code WHERE a.ispopup = '0' AND a.category_code = '$parent_code' AND a.active = '1' AND b.usercode = '".getSessionVar('usercode')."' ORDER BY a.menu_name");
	}
	
	while($r = mysql_fetch_assoc($q)) {
        echo "{ id: '$r[code]', text: '$r[menu_name]', img: 'icon-page' },";
	}

}

?>