<div id="grid" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/inventory/json.item.master.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
				toolbarAdd    : true,
				toolbarDelete : true,
				toolbarEdit   : true
			},
			toolbar: {
                items: [
                    { type: 'break' },
                    { type: 'button', id: 'borrow_logs', caption: 'Borrow Logs', icon: 'w2ui-icon-plus', disabled: true }
                ],
                onClick: function (target, data) {
                    if (target == 'borrow_logs') {
                        var sel = w2ui['grid'].getSelection();
                        var recid = sel[0];

						top.w2popup.open({
							title: 'Borrow Logs',
							style: 'padding:5px; overflow: hidden',
							body: '<iframe src="router.php?p=borrow_logs&itemcode=' + recid + '" class="frame_custom"></iframe>',
							height: '500',
							width: '900'
						});

                    }
                }
            },
			columns: [
				{ field: 'barcode', caption: 'Barcode / Serial', size: '10%' },
                { field: 'model_no', caption: 'Model No', size: '10%' },
                { field: 'description', caption: 'Description', size: '20%' },
                { field: 'category', caption: 'Category', size: '10%' },
                { field: 'brand', caption: 'Brand', size: '10%' },
                { field: 'supplier', caption: 'Supplier', size: '10%' },
                { field: 'unit_cost', caption: 'Unit Cost', size: '10%' },
                { field: 'date_purchased', caption: 'Purchased Date', size: '10%' },
                { field: 'active', caption: 'Phaseout', size: '10%' },

			],
			onAdd: function (event) {
                w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_item_master" class="frame_custom"></iframe>',
                    height: '440',
                });
			},
			onEdit: function (event) {
				var sel = w2ui['grid'].getSelection();
				var recid = sel[0];

                 w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_item_master&code=' + recid + '" class="frame_custom"></iframe>',
                    height: '440'
                });

			},
			onDelete: function (event) {
				event.onComplete = function(){
					 w2alert('Data deleted!');
				}
			},
			onSelect: function(event) {
                w2ui['grid'].toolbar.enable('borrow_logs');
            },
            onUnselect: function(event) {
                w2ui['grid'].toolbar.disable('borrow_logs');
            },
			multiSearch: false,
			searches: [
				{ field: 'barcode', caption: 'Barcode', type: 'text' },
                { field: 'description', caption: 'Description', type: 'text' }
			]
		});
	});
</script>
