<?php

if(isset($_GET['code'])) {
	$code = $_GET['code'];
} else {
	$code = "";
}

if(isset($_POST["insert_category"]) && trim($_POST["name"]) != "") {
	$code        = generateCode('IC');
	$name        = escapeString($_POST["name"]);
	$active      = $_POST['active'];
	
	$q = mysql_query("SELECT code FROM inv_item_category WHERE name = '$name'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
        $code = "";
        $form_submit_status = 2; // duplicate entry
	} else {
        try { 			
			begin();
            
            mysql_query("INSERT INTO inv_item_category (code, name, last_modified_by, last_modified_date, active) VALUES ('$code', '$name', '".getSessionVar('usercode')."', NOW(), '$active')") or die(mysql_error());
            
            commit();
            $form_submit_status = 1;
		} catch(Exception $e) { 
			rollback(); 
			exit();
		}
    }
}

if(isset($_POST["update_category"]) && trim($_POST["name"]) != "" && trim($_POST["code"]) != "") {
	$code        = escapeString($_POST["code"]);
	$name        = escapeString($_POST["name"]);
	$active      = $_POST["active"];
    
    try { 			
        begin(); 
        
        $q = mysql_query("SELECT code FROM inv_item_category WHERE name = '$name'");
        if(mysql_num_rows($q) > 0) {       
            mysql_query("UPDATE inv_item_category SET last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), active = '$active' WHERE code = '$code'") or die(mysql_error());		
        } else {
            mysql_query("UPDATE inv_item_category SET name = '$name', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), active = '$active' WHERE code = '$code'") or die(mysql_error());           
        }
        
        commit();
        $form_submit_status = 1;
    } catch(Exception $e) { 
        rollback(); 
        exit();
    }
}

if($code != "") {
	$q = mysql_query("SELECT * FROM inv_item_category WHERE code = '$code'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
		
	$name        = $r['name'];
	$active      = $r['active'];
} else {
	$name        =
	$active      = "";
}


?>
<?php include_once("index.header.php"); ?>
<div id="form">
<form name="form_category" action="" method="post" >
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <div class="w2ui-page page-0">
        
        <div class="w2ui-field">
            <label>Name:</label>
            <div><input type="text" name="name" maxlength="50" value="<?php echo $name; ?>" required /></div>
        </div>     
        <div class="w2ui-field">
            <label>Active:</label>
            <div>
                <select name="active">
                    <option value="1" <?php if($active == "1") echo "selected"; ?>>Yes</option>
                    <option value="0" <?php if($active == "0") echo "selected"; ?>>No</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>
        <?php if($code != "") { ?>
            <button type="submit" class="btn btn-green" name="update_category">Save</button>            
        <?php } else { ?>
            <button type="submit" class="btn btn-green" name="insert_category">Save</button>
        <?php } ?>
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form').w2form({ 
        name  : 'form',        
        fields: [
           { field: 'name', required: true },
        ]
    });
});
</script>


