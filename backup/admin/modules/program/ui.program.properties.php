<?php

if(isset($_POST["update_properties"]) && trim($_POST["client"]) != "" && trim($_POST["system_name"]) != "" && trim($_POST["system_description"]) != "" && trim($_POST["version"]) != "") {
	$client             = escapeString($_POST['client']);
	$system_name        = escapeString($_POST['system_name']);
	$system_description = escapeString($_POST['system_description']);	
	$version            = escapeString($_POST['version']);	

	query("UPDATE _properties SET client = '$client', system_name = '$system_name', system_description = '$system_description', version = '$version'");	
}

$r = query("SELECT * FROM _properties");
foreach($r as $r) {
	$client             = $r['client'];
	$system_name        = $r['system_name'];
	$system_description = $r['system_description'];
	$version            = $r['version'];
}

?>
<table>
	<thead>
		<tr>
			<th colspan="2"><?php echo $p_menu_name; ?></th>
		</tr>
	</thead>
	<tbody>
    	<form name="form_properties" action="" method="post">
		<tr>
			<th>Client</th>
			<td><input type="text" name="client" style="width:200px;" value="<?php echo $client; ?>" maxlength="100" required /></td>
		</tr>
        <tr>
			<th>System Name</th>
			<td><input type="text" name="system_name" style="width:100px;" value="<?php echo $system_name; ?>" maxlength="50" required /></td>
		</tr>
        <tr>
			<th>System Description</th>
			<td><input type="text" name="system_description" style="width:150px;" value="<?php echo $system_description; ?>" maxlength="100" required /></td>
		</tr>
        <tr>
			<th>Version</th>
			<td><input type="text" name="version" maxlength="50" value="<?php echo $version; ?>" required /></td>
		</tr>
		<tr>
			<td colspan="2">
            	<button type="submit" name="update_properties" title="Save data.">
                	<img src="images/save_16.png" /><span>Save Data</span>
                </button>
                <button type="reset" title="Reset data.">
                	<img src="images/refresh_16.png" /><span>Reset Data</span>
                </button>
            </td>
		</tr>
        </form>
	</tbody>	
</table>