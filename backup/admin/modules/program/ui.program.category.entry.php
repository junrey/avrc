<?php

if(isset($_GET['code'])) {
	$code = $_GET['code'];
} else {
	$code = "";
}

if(isset($_POST["insert_category"]) && trim($_POST["name"]) != "") {
	$code        = generateCode('C');
	$keyword     = escapeString($_POST["keyword"]);
	$name        = escapeString($_POST["name"]);
	$popup_menu  = $_POST['popup_menu'];
	$parent_code = $_POST['parent_code'];
	$active      = $_POST['active'];
	
	$q = mysql_query("SELECT code FROM _category WHERE name = '$name'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
        $code = "";
        $form_submit_status = 2; // duplicate entry
	} else {
       mysql_query("INSERT INTO _category (code, name, keyword, popup_menu, parent_code, last_modified_by, last_modified_date, active) VALUES ('$code', '$name', '$keyword', '$popup_menu', '$parent_code', '".getSessionVar('usercode')."', NOW(), '$active')") or die(mysql_error());	
	   $form_submit_status = 1;
    }
}

if(isset($_POST["update_category"]) && trim($_POST["name"]) != "" && trim($_POST["code"]) != "") {
	$code        = escapeString($_POST["code"]);
	$keyword     = escapeString($_POST["keyword"]);
	$name        = escapeString($_POST["name"]);
	$popup_menu  = $_POST['popup_menu'];
	$parent_code = $_POST['parent_code'];
	$active      = $_POST["active"];
	
	$q = mysql_query("SELECT code FROM _category WHERE name = '$name'");
	if(mysql_num_rows($q) > 0) {
        mysql_query("UPDATE _category SET keyword = '$keyword', popup_menu = '$popup_menu', parent_code = '$parent_code', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), active = '$active' WHERE code = '$code'") or die(mysql_error());	
	} else {
        mysql_query("UPDATE _category SET name = '$name', keyword = '$keyword', popup_menu = '$popup_menu', parent_code = '$parent_code', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW(), active = '$active' WHERE code = '$code'") or die(mysql_error());	
	}
}

if($code != "") {
	$q = mysql_query("SELECT * FROM _category WHERE code = '$code'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
		
	$name        = $r['name'];
	$keyword     = $r['keyword'];
	$popup_menu  = $r['popup_menu'];
	$parent_code = $r['parent_code'];
	$active      = $r['active'];
} else {
	$name        = "";
	$keyword     = "";
	$popup_menu  = "";
	$parent_code = "";
	$active      = "";
}


?>
<?php include_once("index.header.php"); ?>
<div id="form">
<form name="form_category" action="" method="post" >
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <div class="w2ui-page page-0">
        <div class="w2ui-field">
            <label>Keyword:</label>
            <div><input type="text" name="keyword" value="<?php echo $keyword; ?>" maxlength="30" /></div>
        </div>
        <div class="w2ui-field">
            <label>Name:</label>
            <div><input type="text" name="name" maxlength="30" value="<?php echo $name; ?>" style="width:200px;" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Popup Menu:</label>
            <div>
                <select name="popup_menu">				
                    <option value="0" <?php if($popup_menu == "0") echo "selected"; ?>>No</option>
                    <option value="1" <?php if($popup_menu == "1") echo "selected"; ?>>Yes</option>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Parent Menu:</label>
            <div>
                <select name="parent_code">
                    <option value="0">Parent</option>
                    <?php $q = mysql_query("SELECT code, name FROM _category WHERE active = '1' ORDER BY name") or die(mysql_error()); ?>
                    <?php while($r = mysql_fetch_assoc($q)) { ?>
                            <option value="<?php echo $r['code']; ?>" <?php if($parent_code == $r['code']) echo "selected"; ?>><?php echo $r['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Active:</label>
            <div>
                <select name="active">
                    <option value="1" <?php if($active == "1") echo "selected"; ?>>Yes</option>
                    <option value="0" <?php if($active == "0") echo "selected"; ?>>No</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>
        <?php if($code != "") { ?>
            <button type="submit" class="btn btn-green" name="update_category">Save</button>            
        <?php } else { ?>
            <button type="submit" class="btn btn-green" name="insert_category">Save</button>
        <?php } ?>
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form').w2form({ 
        name  : 'form',        
        fields: [
           { field: 'name', required: true },
        ]
    });
});
</script>


