<div id="grid" style="width:100%; height:100%;"></div>

<?php 
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]); 
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({ 
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/program/json.program.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
				toolbarAdd    : true,
				toolbarDelete : true,
				toolbarEdit   : true
			},      
			columns: [
				{ field: 'menu_name', caption: 'Menu Name', size: '15%' },
				{ field: 'keyword', caption: 'Keyword', size: '15%' },
				{ field: 'filename', caption: 'Filename', size: '40%' },
				{ field: 'category', caption: 'Category', size: '15%' },
                { field: 'width', caption: 'Width', size: '10%' },
                { field: 'height', caption: 'Height', size: '10%' },
                { field: 'ispopup', caption: 'Popup', size: '10%' },
				{ field: 'active', caption: 'Active', size: '10%' },
				
			],
			onAdd: function (event) {
                w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_program" class="frame_custom"></iframe>',
                    height: '400',
                });
			},
			onEdit: function (event) {
				var sel = w2ui['grid'].getSelection();
				var recid = sel[0];
               
                 w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_program&code=' + recid + '" class="frame_custom"></iframe>',
                    height: '400',
                });

			},
			onDelete: function (event) {
				event.onComplete = function(){
					w2alert('Data deleted!');	
				}
			},
			multiSearch: false,
			searches: [
				{ field: 'name', caption: 'Name', type: 'text' }
			]
		});    
	});
</script>
