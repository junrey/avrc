<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=borrowing_report_teacher_" . Date("mdY") . ".xls");

$sem  = getSessionVar('sem');
if(isset($_GET['sem'])){
    $sem = escapeString($_GET['sem']);
}

$term = getSessionVar('term');
if(isset($_GET['term'])){
    $term = escapeString($_GET['term']);
}

$ay   = getSessionVar('ay');
if(isset($_GET['ay'])){
    $ay = escapeString($_GET['ay']);
}

setUTF8();

$q = mysql_query("SELECT a.*, b.ay, CASE b.sem WHEN '1' THEN '1st' WHEN '2' THEN '2nd' WHEN '3' THEN 'Summer' END AS sem FROM borrow_item AS a INNER JOIN borrow_header AS b ON a.borrow_header = b.code WHERE a.borrow_header <> '' AND a.void = '0' AND b.sem ='$sem' AND b.ay = '$ay' AND b.isstudent = '0' ORDER BY a.borrow_date DESC") or die(mysql_error());

?>
<table border="1">
    <tr>
        <th>Borrowed Date</th>
        <th>ID Number</th>
        <th>Borrower's Name</th>
        <th>College</th>
        <th>Subject</th>
        <th>Remarks</th>
        <th>Barcode</th>
        <th>Item</th>
        <th>Venue</th>
        <th>Served By</th>
        <th>Return Status</th>
        <th>Date Returned</th>
        <th>Received By</th>
        <th>Semester</th>
        <th>Academic Year</th>
    </tr>
    <?php while($r = mysql_fetch_assoc($q)) {
		$q3 = mysql_query("SELECT c.lname, c.fname, c.coll FROM teacher AS c WHERE c.idnumber = '$r[borrower]' LIMIT 1") or die(mysql_error());
		$r3 = mysql_fetch_assoc($q3);

		$borrower = strtoupper($r3['lname']) . ", " .strtoupper($r3['fname']);

        $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
        $r2 = mysql_fetch_assoc($q2);

        $status    = "Yes";
        $status_bg = "";

        if($r['item_return'] == "0") {
            $status    = "No";
        }

    ?>
    <tr>
        <td><?php echo datetime("m/j/y h:i:s a", $r['borrow_date']); ?></td>
        <td><?php echo "&nbsp;" . $r['borrower']; ?></td>
        <td><?php echo htmlentities($borrower, ENT_QUOTES); ?></td>
        <td><?php echo $r3['coll']; ?></td>
        <td><?php echo htmlentities($r['subject'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['remarks'], ENT_QUOTES); ?></td>
        <td><?php echo "&nbsp;" . $r['barcode']; ?></td>
        <td><?php echo htmlentities($r2['description'], ENT_QUOTES); ?></td>
        <td><?php echo $r['venue']; ?></td>
        <td><?php echo htmlentities(getUserLogName($r['usercode']), ENT_QUOTES); ?></td>
        <td><?php echo $status; ?></td>
        <?php
			if($r['a_return_date'] == "0000-00-00 00:00:00") {
				$return_date = "";	
			} else {
				$return_date = datetime("m/j/y h:i:s a", $r['a_return_date']);
			}
		?>
        <td><?php echo $return_date; ?></td>
        <td><?php echo htmlentities(getUserLogName($r['rusercode']), ENT_QUOTES) ?></td>
        <td><?php echo $r['sem']; ?></td>
        <td><?php echo $r['ay']; ?></td>
    </tr>
    <?php } ?>
</table>
