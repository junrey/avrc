<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=unreturn_item_report_" . Date("mdY") . ".xls");

setUTF8();

$q  = mysql_query("SELECT * FROM borrow_item WHERE borrow_header <> '' AND void = '0' AND item_return <> '1' ORDER BY borrow_date DESC") or die(mysql_error());

?>
<table border="1">
    <tr>
        <th>Borrowed Date</th>
        <th>ID Number</th>
        <th>Borrower's Name</th>
        <th>Teacher</th>
        <th>Subject</th>
        <th>Remarks</th>
        <th>Barcode</th>
        <th>Item</th>
        <th>Venue</th>
        <th>Served By</th>
    </tr>
    <?php while($r = mysql_fetch_assoc($q)) {
    		$borrower = "";
    		$q2 = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$r[borrower]'");
    		if(mysql_num_rows($q2) > 0) {
    			$r2 = mysql_fetch_assoc($q2);
    			$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

    		} else {
    			$q2 = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$r[borrower]'");
    			if(mysql_num_rows($q2) > 0) {
    				$r2 = mysql_fetch_assoc($q2);
    				$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

    			} else {
    				$borrower = $r['borrower'];
    			}

    		}

            $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
            $r2 = mysql_fetch_assoc($q2);
    ?>
    <tr>
        <td><?php echo datetime("m/j/y h:i:s a", $r['borrow_date']); ?></td>
        <td><?php echo "&nbsp;" . $r['borrower']; ?></td>
        <td><?php echo htmlentities($borrower, ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['teacher'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['subject'], ENT_QUOTES); ?></td>
        <td><?php echo htmlentities($r['remarks'], ENT_QUOTES); ?></td>
        <td><?php echo "&nbsp;" . $r['barcode']; ?></td>
        <td><?php echo htmlentities($r2['description'], ENT_QUOTES); ?></td>
        <td><?php echo $r['venue']; ?></td>
        <td><?php echo htmlentities(getUserLogName($r['usercode']), ENT_QUOTES); ?></td>
    </tr>
    <?php } ?>
</table>
