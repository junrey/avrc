<?php

if(isset($_GET['usercode'])) {
	$usercode = $_GET['usercode'];
} else {
	$usercode = "";
}

if(isset($_POST["insert_user_info"]) && trim($_POST["username"]) != "" && trim($_POST["password"]) != "" && trim($_POST["lname"]) != "" && trim($_POST["fname"]) != "" && trim($_POST["mname"]) != "") {
	$usercode  = generateCode('UI');
	$username  = escapeString($_POST['username']);
	$password  = md5(escapeString($_POST['password']));
	$idnumber  = escapeString($_POST['idnumber']);
	$lname     = escapeString($_POST['lname']);
	$fname     = escapeString($_POST['fname']);
	$mname     = escapeString($_POST['mname']);
	$active    = $_POST['active'];
	
	$q = mysql_query("SELECT username FROM _user WHERE username = '$username'") or die(mysql_error());
    if(mysql_num_rows($q) > 0) {
        $usercode = "";
        $form_submit_status = 2; // duplicate entry
    } else {
        mysql_query("INSERT INTO _user (usercode, username, password, lname, fname, mname, active, last_modified_by, last_modified_date) VALUES ('$usercode', '$username', '$password', '$lname', '$fname', '$mname', '$active','".getSessionVar('usercode')."',NOW())") or die(mysql_error());
        if($idnumber != "") {
			mysql_query("UPDATE _user SET idnumber = '$idnumber' WHERE usercode = '$usercode'") or die(mysql_error());
		}
        $form_submit_status = 1;
    }
}

if(isset($_POST["update_user_info"]) && trim($_POST["usercode"]) != "" && trim($_POST["username"]) != "" && trim($_POST["lname"]) != "" && trim($_POST["fname"]) != "" && trim($_POST["mname"]) != "") {
	$usercode = escapeString($_POST['usercode']);
	$username = escapeString($_POST['username']);
	$password = escapeString($_POST['password']);
	$idnumber = escapeString($_POST['idnumber']);
	$lname    = escapeString($_POST['lname']);
	$fname    = escapeString($_POST['fname']);
	$mname    = escapeString($_POST['mname']);
	$active   = $_POST['active'];
	
	$q = mysql_query("SELECT username FROM _user WHERE username = '$username'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {
 		mysql_query("UPDATE _user SET lname = '$lname', fname = '$fname', mname = '$mname', active = '$active', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW() WHERE usercode = '$usercode'") or die(mysql_error());    
	} else {
        mysql_query("UPDATE _user SET username = '$username', idnumber = '$idnumber', lname = '$lname', fname = '$fname', mname = '$mname', active = '$active', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW() WHERE usercode = '$usercode'") or die(mysql_error());
	}
	
	// password checking
	$q = mysql_query("SELECT username FROM _user WHERE usercode = '$usercode' AND password = '$password'") or die(mysql_error());
    if(mysql_num_rows($q) == 0) {
		$password = md5($password);
		mysql_query("UPDATE _user SET password = '$password' WHERE usercode = '$usercode'") or die(mysql_error());
	}
	
	if($idnumber != "") {
		mysql_query("UPDATE _user SET idnumber = '$idnumber' WHERE usercode = '$usercode'") or die(mysql_error());
	}
    
     $form_submit_status = 1;

}


if($usercode != "") {
	$q = mysql_query("SELECT * FROM _user WHERE usercode = '$usercode'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
		
	$idnumber = $r['idnumber'];
    $username = $r['username'];
	$password = $r['password'];
    $lname    = $r['lname'];
    $fname    = $r['fname'];
	$mname    = $r['mname'];
    $active   = $r['active'];
} else {
	$idnumber = 
    $username = 
	$password = 
    $lname    = 
    $fname    = 
	$mname    = 
	$active   = "";
}


?>
<?php include_once("index.header.php"); ?>
<div id="form">
<form name="form" action="" method="post" >
    <input type="hidden" name="usercode" value="<?php echo $usercode; ?>">
    <div class="w2ui-page page-0">     
        <div class="w2ui-field">
            <label>Username:</label>
            <div><input type="text" name="username" maxlength="20" value="<?php echo $username; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Password:</label>
            <div><input type="password" name="password" maxlength="200" value="<?php echo $password; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>ID Number:</label>
            <div><input type="text" name="idnumber" maxlength="10" value="<?php echo $idnumber; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>First Name:</label>
            <div><input type="text" name="fname" maxlength="30" value="<?php echo $fname; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Last Name:</label>
            <div><input type="text" name="lname" maxlength="30" value="<?php echo $lname; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Middle Name:</label>
            <div><input type="text" name="mname" maxlength="30" value="<?php echo $mname; ?>" required /></div>
        </div> 
        <div class="w2ui-field">
            <label>Active:</label>
            <div>
                <select name="active">
                    <option value="1" <?php if($active == "1") echo "selected"; ?>>Yes</option>
                    <option value="0" <?php if($active == "0") echo "selected"; ?>>No</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>
        <?php if($usercode == "") { ?>
            <button type="submit" class="btn btn-green" name="insert_user_info">Save</button>            
        <?php } else { ?>
            <button type="submit" class="btn btn-green" name="update_user_info">Save</button>
        <?php } ?>
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form').w2form({ 
        name  : 'form',        
        fields: [
            { field: 'username', required: true },
            { field: 'password', required: true },
            { field: 'lname', required: true },
            { field: 'fname', required: true },
            { field: 'mname', required: true },
        ]
    });
});
</script>


