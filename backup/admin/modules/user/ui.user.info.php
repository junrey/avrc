<div id="grid" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/user/json.user.info.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
				toolbarAdd    : true,
				toolbarDelete : true,
				toolbarEdit   : true
			},
            toolbar: {
                items: [
                    { type: 'break' },
                    { type: 'button', id: 'access_rights', caption: 'Access Rights', icon: 'w2ui-icon-plus', disabled: true }
                ],
                onClick: function (target, data) {
                    if (target == 'access_rights') {
                        var sel = w2ui['grid'].getSelection();
                        var recid = sel[0];

						if(recid == "<?php echo getSessionVar('usercode'); ?>") {
							w2alert('Editing your own rights is invalid!');
						} else {
	                         w2popup.open({
	                            title: '<?php echo $p_menu_name; ?>',
	                            style: 'padding:5px; overflow: hidden',
	                            body: '<iframe src="router.php?p=user_access&usercode=' + recid + '" class="frame_custom"></iframe>',
	                            height: '400',
	                            width: '800'
	                        });
						}
                    }
                }
            },
			columns: [
                { field: 'username', caption: 'Username', size: '10%' },
                { field: 'idnumber', caption: 'ID Number', size: '10%' },
				{ field: 'fname', caption: 'First Name', size: '10%' },
                { field: 'lname', caption: 'Last Name', size: '10%' },
                { field: 'mname', caption: 'Middle Name', size: '10%' },
				{ field: 'active', caption: 'Active', size: '10%' },

			],
			onAdd: function (event) {
                w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_user_info" class="frame_custom"></iframe>',
                    height: '380',
                });
			},
			onEdit: function (event) {
				var sel = w2ui['grid'].getSelection();
				var recid = sel[0];

                 w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_user_info&usercode=' + recid + '" class="frame_custom"></iframe>',
                    height: '380'
                });

			},
			onDelete: function (event) {
				event.onComplete = function(){
					 w2alert('Data deleted!');
				}
			},
            onSelect: function(event) {
                w2ui['grid'].toolbar.enable('access_rights');
            },
            onUnselect: function(event) {
                w2ui['grid'].toolbar.disable('access_rights');
            },
			multiSearch: false,
			searches: [
				{ field: 'lname', caption: 'Last Name', type: 'text' },
                { field: 'idnumber', caption: 'ID Number', type: 'text' }
			]
		});
	});
</script>
