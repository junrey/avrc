<?php

if(isset($_GET['row_id'])) {
	$row_id = $_GET['row_id'];
} else {
	$row_id = "";
}

if(isset($_POST["insert_user_info"]) && trim($_POST["idnumber"]) != "" && trim($_POST["lname"]) != "" && trim($_POST["fname"]) != "") {

	$idnumber = escapeString($_POST['idnumber']);
	$lname    = escapeString($_POST['lname']);
	$fname    = escapeString($_POST['fname']);
	$mname    = escapeString($_POST['mname']);
	$dept     = $_POST['dept'];
	$coll     = $_POST['coll'];
	
	$q = mysql_query("SELECT idnumber FROM teacher WHERE idnumber = '$idnumber'") or die(mysql_error());
    if(mysql_num_rows($q) > 0) {
        $row_id = "";
        $form_submit_status = 2; // duplicate entry
    } else {
		try { 			
			begin();
       		
			mysql_query("INSERT INTO teacher (idnumber, lname, fname, mname, dept, coll) VALUES ('$idnumber', '$lname', '$fname', '$mname', '$dept', '$coll')") or die(mysql_error());
        	
			$q2 = mysql_query("SELECT row_id FROM teacher WHERE idnumber = '$idnumber'") or die(mysql_error());
			$r2 = mysql_fetch_assoc($q2);
			$row_id = $r2['row_id'];
			
			commit();
			$form_submit_status = 1;
		} catch(Exception $e) { 
			rollback(); 
			exit();
		}
    }
}

if(isset($_POST["update_user_info"]) && trim($_POST["row_id"]) != "" && trim($_POST["idnumber"]) != "" && trim($_POST["lname"]) != "" && trim($_POST["fname"]) != "") {
	$row_id   = escapeString($_POST['row_id']);
	$idnumber = escapeString($_POST['idnumber']);
	$lname    = escapeString($_POST['lname']);
	$fname    = escapeString($_POST['fname']);
	$mname    = escapeString($_POST['mname']);
	$dept     = $_POST['dept'];
	$coll     = $_POST['coll'];
	
	try { 			
		begin();
       		
 		mysql_query("UPDATE teacher SET idnumber = '$idnumber', lname = '$lname', fname = '$fname', mname = '$mname', dept = '$dept', coll = '$coll' WHERE row_id = '$row_id'") or die(mysql_error());    
		
		commit();
		$form_submit_status = 1;
	} catch(Exception $e) { 
		rollback(); 
		exit();
	}
}


if($row_id != "") {
	$q = mysql_query("SELECT * FROM teacher WHERE row_id = '$row_id'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
		
	$idnumber = $r['idnumber'];
    $lname    = $r['lname'];
    $fname    = $r['fname'];
	$mname    = $r['mname'];
    $dept     = $r['dept'];
	$coll     = $r['coll'];
} else {
	$idnumber =
    $lname    =
    $fname    =
	$mname    =
    $dept     =
	$coll     = "";
}


?>
<?php include_once("index.header.php"); ?>
<div id="form">
<form name="form" action="" method="post" >
    <input type="hidden" name="row_id" value="<?php echo $row_id; ?>">
    <div class="w2ui-page page-0">     
        <div class="w2ui-field">
            <label>Idnumber:</label>
            <div><input type="text" name="idnumber" maxlength="7" value="<?php echo $idnumber; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>First Name:</label>
            <div><input type="text" name="fname" maxlength="30" value="<?php echo $fname; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Last Name:</label>
            <div><input type="text" name="lname" maxlength="30" value="<?php echo $lname; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Middle Name:</label>
            <div><input type="text" name="mname" maxlength="30" value="<?php echo $mname; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Department:</label>
            <div>
                 <select name="dept">
                 	<option value="" <?php if($dept == "") echo "selected"; ?>>Select</option>
                 	<?php $q2 = mysql_query("SELECT DISTINCT dept FROM teacher ORDER BY dept") or die(mysql_error()); ?>
					<?php while($r2 = mysql_fetch_assoc($q2)) { ?>
                 		<option value="<?php echo $r2['dept']; ?>" <?php if($dept == $r2['dept']) echo "selected"; ?>><?php echo $r2['dept']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
         <div class="w2ui-field">
            <label>College:</label>
            <div>
                <select name="coll">
                	<option value="" <?php if($coll == "") echo "selected"; ?>>Select</option>
                	<option value="BSN" <?php if($coll == "BSN") echo "selected"; ?>>BSN</option>
                    <option value="CAS" <?php if($coll == "CAS") echo "selected"; ?>>CAS</option>
                    <option value="CBA" <?php if($coll == "CBA") echo "selected"; ?>>CBA</option>
                    <option value="EDU" <?php if($coll == "EDU") echo "selected"; ?>>EDU</option>
                    <option value="ENG" <?php if($coll == "ENG") echo "selected"; ?>>ENG</option>
                    <option value="LAW" <?php if($coll == "LAW") echo "selected"; ?>>LAW</option>
                    <option value="NONE" <?php if($coll == "NONE") echo "selected"; ?>>NONE</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>
        <?php if($row_id == "") { ?>
            <button type="submit" class="btn btn-green" name="insert_user_info">Save</button>            
        <?php } else { ?>
            <button type="submit" class="btn btn-green" name="update_user_info">Save</button>
        <?php } ?>
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form').w2form({ 
        name  : 'form',        
        fields: [
            { field: 'idnumber', required: true },
            { field: 'lname', required: true },
            { field: 'fname', required: true },
            { field: 'mname', required: true },
        ]
    });
});
</script>


