<?php
set_time_limit(0);

if(isset($_POST["update_db"])) {
	try {
		begin();
		/* ODBC Connection */
		$db_itc_student  = "student";
		$db_itc_teachers = "teachers";
		$conn_odbc_itc   = odbc_connect("Driver={SQL Server};Server=172.22.3.22;Database=$db_itc_student;", "sa", "a");

		$q = odbc_exec($conn_odbc_itc,"SELECT sem_db FROM $db_itc_teachers.dbo.config");
		$r = odbc_fetch_array($q);
		$xr = trim($r['sem_db']);

		$s2 = "SELECT DISTINCT s.idnumb,s.lnamee,s.fnamee,s.mnamee,s.course,s.majorr,s.year,s.sectio,s.sexxxx,s.parent,s.address,s.telno,s.s_telno,s.coll FROM $db_itc_student.dbo.student AS s INNER JOIN $xr.dbo.grades AS g ON s.idnumb = g.idnumb WHERE g.wdrawn = '0' ORDER BY s.year, s.course, s.majorr, s.sectio, s.lnamee, s.fnamee, s.mnamee";
		$q2 = odbc_exec($conn_odbc_itc,$s2);

		/* MSSQL Connection */
		//$q = mssql_query("SELECT sem_db FROM $db_itc_teachers.dbo.config",$link_itc);
		//$r = mssql_fetch_assoc($q);
		//$xr = trim($r['sem_db']);

		//$s2 = "SELECT DISTINCT s.idnumb,s.lnamee,s.fnamee,s.mnamee,s.course,s.majorr,s.year,s.sectio,s.sexxxx,s.parent,s.address,s.telno,s.s_telno FROM $db_itc_student.dbo.student AS s INNER JOIN $xr.dbo.grades AS g ON s.idnumb = g.idnumb WHERE g.wdrawn = '0' ORDER BY s.year, s.course, s.majorr, s.sectio, s.lnamee, s.fnamee, s.mnamee";
		//$q2 = mssql_query($s2,$link_itc);


		while($r2 = odbc_fetch_array($q2)) {
			$idnumber = trim($r2['idnumb']);
			$lname    = mysql_real_escape_string(trim($r2['lnamee']));
			$fname    = mysql_real_escape_string(trim($r2['fnamee']));
			$mname    = mysql_real_escape_string(trim($r2['mnamee']));
			$course   = mysql_real_escape_string(trim($r2['course']));
			$major    = mysql_real_escape_string(trim($r2['majorr']));
			$year     = mysql_real_escape_string(trim($r2['year']));
			$section  = mysql_real_escape_string(trim($r2['sectio']));
			$address  = mysql_real_escape_string(trim($r2['address']));

			if(trim($r2['s_telno']) != "") {
				$telno = mysql_real_escape_string(trim($r2['s_telno']));
			} else {
				$telno = mysql_real_escape_string(trim($r2['telno']));
			}

			$parent   = mysql_real_escape_string(trim($r2['parent']));
			$gender   = mysql_real_escape_string(trim($r2['sexxxx']));
			$coll     = mysql_real_escape_string(trim($r2['coll']));


			$q4 = mysql_query("SELECT idnumber FROM student WHERE idnumber = '$idnumber'") or die(mysql_error());
			if(mysql_num_rows($q4) == 0) {
				$s3 = "INSERT INTO student(idnumber,lname,fname,mname,course,major,year,section,address,telno,parent,gender,coll) VALUES ('$idnumber','$lname','$fname','$mname','$course','$major','$year','$section','$address','$telno','$parent','$gender','$coll')";
				$q3 = mysql_query($s3) or die(mysql_error());
			} else {
				$s3 = "UPDATE student SET lname='$lname',fname='$fname',mname='$mname',course='$course',major='$major',year='$year',section='$section',address='$address',telno='$telno',parent='$parent',gender='$gender',coll='$coll' WHERE idnumber='$idnumber'";
				$q3 = mysql_query($s3) or die(mysql_error());
			}

		}

		mysql_query("UPDATE config SET student_db_update = NOW(), student_db_update_by = '".getSessionVar('usercode')."'") or die(mysql_error());

		commit();

	} catch(Exception $e) {
		rollback();
		exit();
	}
}

$q5 = mysql_query("SELECT student_db_update, student_db_update_by FROM config") or die(mysql_error());
$r5 = mysql_fetch_assoc($q5);

?>
<?php include_once("index.header.php"); ?>
<div id="form" style="width:500px;">
    <form name="form_update_db" action="" method="post">
    <div class="w2ui-page page-0">
        <div class="w2ui-field">
            <label>Last Update:</label>
            <div style="padding-top:7px;"><?php echo datetime("M j Y g:i:s a", $r5['student_db_update']); ?></div>
        </div>
        <div class="w2ui-field">
            <label>By:</label>
            <div style="padding-top:7px;"><?php echo getUserLogName($r5['student_db_update_by']); ?></div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="submit" name="update_db" class="btn btn-green" />Update Student Database</button>
    </div>
    </form>
</div>
<script type="text/javascript">
$(function () {
    $('#form').w2form({
        name  : 'form',
        header : '<?php echo $p_menu_name; ?>',
    });
});
</script>
