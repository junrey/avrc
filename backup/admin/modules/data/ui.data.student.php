<div id="grid" style="width:100%; height:100%;"></div>

<?php 
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]); 
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({ 
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/data/json.data.student.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
				toolbarAdd    : true,
				//toolbarDelete : true,
				toolbarEdit   : true
			},
			toolbar: {
                items: [
                    { type: 'break' },
                    { type: 'button', id: 'borrow_logs', caption: 'Borrow Logs', icon: 'w2ui-icon-plus', disabled: true }
                ],
                onClick: function (target, data) {
                    if (target == 'borrow_logs') {
                        var sel = w2ui['grid'].getSelection();
                        var recid = sel[0];
						var idnumber = w2ui['grid'].get(sel)["idnumber"];

						top.w2popup.open({
							title: 'Borrow Logs',
							style: 'padding:5px; overflow: hidden',
							body: '<iframe src="router.php?p=data_borrow_logs&idnumber=' + idnumber + '" class="frame_custom"></iframe>',
							height: '500',
							width: '900'
						});

                    }
                }
            },    
			columns: [
				{ field: 'idnumber', caption: 'ID Number', size: '10%' },
                { field: 'lname', caption: 'Last Name', size: '20%' },
                { field: 'fname', caption: 'First Name', size: '20%' },
                { field: 'mname', caption: 'Middle Name', size: '20%' },
                { field: 'course', caption: 'Course', size: '10%' },
                { field: 'major', caption: 'Major', size: '10%' },
                { field: 'year', caption: 'Year', size: '10%' },
                { field: 'section', caption: 'Section', size: '10%' },
                { field: 'coll', caption: 'College', size: '10%' },
				
			],
			onAdd: function (event) {
                w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_student" class="frame_custom"></iframe>',
                    height: '560',
                });
			},
			onEdit: function (event) {
				var sel = w2ui['grid'].getSelection();
				var recid = sel[0];
               
                 w2popup.open({
                    title: '<?php echo $p_menu_name; ?>',
                    style: 'padding:5px; overflow: hidden',
                    body: '<iframe src="router.php?p=entry_student&row_id=' + recid + '" class="frame_custom"></iframe>',
                    height: '560'
                });

			},
			onDelete: function (event) {
				event.onComplete = function(){
					 w2alert('Data deleted!');	
				}
			},
			onSelect: function(event) {
                w2ui['grid'].toolbar.enable('borrow_logs');
            },
            onUnselect: function(event) {
                w2ui['grid'].toolbar.disable('borrow_logs');
            },
			multiSearch: false,
			searches: [
				{ field: 'idnumber', caption: 'ID Number', type: 'text' },
                { field: 'lname', caption: 'Last Name', type: 'text' }
			]
		});    
	});
</script>
