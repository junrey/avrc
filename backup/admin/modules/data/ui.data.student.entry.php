<?php

if(isset($_GET['row_id'])) {
	$row_id = $_GET['row_id'];
} else {
	$row_id = "";
}

if(isset($_POST["insert_user_info"]) && trim($_POST["idnumber"]) != "" && trim($_POST["lname"]) != "" && trim($_POST["fname"]) != "") {

	$idnumber = escapeString($_POST['idnumber']);
	$lname    = escapeString($_POST['lname']);
	$fname    = escapeString($_POST['fname']);
	$mname    = escapeString($_POST['mname']);
	$course   = $_POST['course'];
	$major    = $_POST['major'];
	$year     = $_POST['year'];
	$section  = $_POST['section'];
	$address  = $_POST['address'];
	$telno    = $_POST['telno'];
	$parent   = $_POST['parent'];
	$gender   = $_POST['gender'];
	$coll     = $_POST['coll'];
	
	$q = mysql_query("SELECT idnumber FROM student WHERE idnumber = '$idnumber'") or die(mysql_error());
    if(mysql_num_rows($q) > 0) {
        $row_id = "";
        $form_submit_status = 2; // duplicate entry
    } else {
		try { 			
			begin();
       		
			mysql_query("INSERT INTO student (idnumber, lname, fname, mname, course, major, year, section, address, telno, parent, gender, coll) VALUES ('$idnumber', '$lname', '$fname', '$mname', '$course', '$major', '$year', '$section', '$address', '$telno', '$parent', '$gender', '$coll')") or die(mysql_error());
        	
			$q2 = mysql_query("SELECT row_id FROM student WHERE idnumber = '$idnumber'") or die(mysql_error());
			$r2 = mysql_fetch_assoc($q2);
			$row_id = $r2['row_id'];
			
			commit();
			$form_submit_status = 1;
		} catch(Exception $e) { 
			rollback(); 
			exit();
		}
    }
}

if(isset($_POST["update_user_info"]) && trim($_POST["row_id"]) != "" && trim($_POST["idnumber"]) != "" && trim($_POST["lname"]) != "" && trim($_POST["fname"]) != "") {
	$row_id   = escapeString($_POST['row_id']);
	$idnumber = escapeString($_POST['idnumber']);
	$lname    = escapeString($_POST['lname']);
	$fname    = escapeString($_POST['fname']);
	$mname    = escapeString($_POST['mname']);
	$course   = $_POST['course'];
	$major    = $_POST['major'];
	$year     = $_POST['year'];
	$section  = $_POST['section'];
	$address  = $_POST['address'];
	$telno    = $_POST['telno'];
	$parent   = $_POST['parent'];
	$gender   = $_POST['gender'];
	$coll     = $_POST['coll'];
	
	try { 			
		begin();
       		
 		mysql_query("UPDATE student SET idnumber = '$idnumber', lname = '$lname', fname = '$fname', mname = '$mname', course = '$course', major = '$major', year = '$year', section = '$section', address = '$address', telno = '$telno', parent = '$parent', gender = '$gender', coll = '$coll' WHERE row_id = '$row_id'") or die(mysql_error());    
		
		commit();
		$form_submit_status = 1;
	} catch(Exception $e) { 
		rollback(); 
		exit();
	}
}


if($row_id != "") {
	$q = mysql_query("SELECT * FROM student WHERE row_id = '$row_id'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
		
	$idnumber = $r['idnumber'];
    $lname    = $r['lname'];
    $fname    = $r['fname'];
	$mname    = $r['mname'];
    $course   = $r['course'];
    $major    = $r['major'];
	$year     = $r['year'];
	$section  = $r['section'];
	$address  = $r['address'];
	$telno    = $r['telno'];
	$parent   = $r['parent'];
	$gender   = $r['gender'];
	$coll     = $r['coll'];
} else {
	$idnumber =
    $lname    =
    $fname    =
	$mname    =
    $course   =
    $major    =
	$year     =
	$section  =
	$address  =
	$telno    =
	$parent   =
	$gender   =
	$coll     = "";
}


?>
<?php include_once("index.header.php"); ?>
<div id="form">
<form name="form" action="" method="post" >
    <input type="hidden" name="row_id" value="<?php echo $row_id; ?>">
    <div class="w2ui-page page-0">     
        <div class="w2ui-field">
            <label>Idnumber:</label>
            <div><input type="text" name="idnumber" maxlength="7" value="<?php echo $idnumber; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>First Name:</label>
            <div><input type="text" name="fname" maxlength="30" value="<?php echo $fname; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Last Name:</label>
            <div><input type="text" name="lname" maxlength="30" value="<?php echo $lname; ?>" required /></div>
        </div>
        <div class="w2ui-field">
            <label>Middle Name:</label>
            <div><input type="text" name="mname" maxlength="30" value="<?php echo $mname; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Course:</label>
            <div>
                 <select name="course">
                 	<option value="" <?php if($course == "") echo "selected"; ?>>Select</option>
                    <option value="AB" <?php if($course == "AB") echo "selected"; ?>>AB</option>
                    <option value="BA" <?php if($course == "BA") echo "selected"; ?>>BA</option>
                    <option value="BC" <?php if($course == "BC") echo "selected"; ?>>BC</option>
                    <option value="BD" <?php if($course == "BD") echo "selected"; ?>>BD</option>
                    <option value="BE" <?php if($course == "BE") echo "selected"; ?>>BE</option>
                    <option value="BL" <?php if($course == "BL") echo "selected"; ?>>BL</option>
                    <option value="BN" <?php if($course == "BN") echo "selected"; ?>>BN</option>
                    <option value="BS" <?php if($course == "BS") echo "selected"; ?>>BS</option>
                    <option value="CO" <?php if($course == "CO") echo "selected"; ?>>CO</option>
                    <option value="DOE" <?php if($course == "DOE") echo "selected"; ?>>DOE</option>
                    <option value="EN" <?php if($course == "EN") echo "selected"; ?>>EN</option>
                    <option value="JD" <?php if($course == "JD") echo "selected"; ?>>JD</option>
                    <option value="LC" <?php if($course == "LC") echo "selected"; ?>>LC</option>
                    <option value="MAT" <?php if($course == "MAT") echo "selected"; ?>>MAT</option>
                    <option value="MBA" <?php if($course == "MBA") echo "selected"; ?>>MBA</option>
                    <option value="MCR" <?php if($course == "MCR") echo "selected"; ?>>MCR</option>
                    <option value="MD" <?php if($course == "MD") echo "selected"; ?>>MD</option>
                    <option value="MEC" <?php if($course == "MEC") echo "selected"; ?>>MEC</option>
                    <option value="MED" <?php if($course == "MED") echo "selected"; ?>>MED</option>
                    <option value="MEE" <?php if($course == "MEE") echo "selected"; ?>>MEE</option>
                    <option value="MEM" <?php if($course == "MEM") echo "selected"; ?>>MEM</option>
                    <option value="MIT" <?php if($course == "MIT") echo "selected"; ?>>MIT</option>
                    <option value="MN" <?php if($course == "MN") echo "selected"; ?>>MN</option>
                    <option value="MP" <?php if($course == "MP") echo "selected"; ?>>MP</option>
                    <option value="MPM" <?php if($course == "MPM") echo "selected"; ?>>MPM</option>
                    <option value="PAL" <?php if($course == "PAL") echo "selected"; ?>>PAL</option>
                    <option value="PBM" <?php if($course == "PBM") echo "selected"; ?>>PBM</option>
                    <option value="PDS" <?php if($course == "PDS") echo "selected"; ?>>PDS</option>
                    <option value="PED" <?php if($course == "PED") echo "selected"; ?>>PED</option>
                    <option value="PHD" <?php if($course == "PHD") echo "selected"; ?>>PHD</option>
                    <option value="PME" <?php if($course == "PME") echo "selected"; ?>>PME</option>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Major:</label>
            <div>
                 <select name="major">
                 	<option value="" <?php if($major == "") echo "selected"; ?>>Select</option>
                    <option value="AB" <?php if($major == "AB") echo "selected"; ?>>AB</option>
                    <option value="AC" <?php if($major == "AC") echo "selected"; ?>>AC</option>
                    <option value="AG" <?php if($major == "AG") echo "selected"; ?>>AG</option>
                    <option value="AR" <?php if($major == "AR") echo "selected"; ?>>AR</option>
                    <option value="AT" <?php if($major == "AT") echo "selected"; ?>>AT</option>
                    <option value="AU" <?php if($major == "AU") echo "selected"; ?>>AU</option>
                    <option value="BA" <?php if($major == "BA") echo "selected"; ?>>BA</option>
                    <option value="BE" <?php if($major == "BE") echo "selected"; ?>>BE</option>
                    <option value="BI" <?php if($major == "BI") echo "selected"; ?>>BI</option>
                    <option value="BM" <?php if($major == "BM") echo "selected"; ?>>BM</option>
                    <option value="BN" <?php if($major == "BN") echo "selected"; ?>>BN</option>
                    <option value="CE" <?php if($major == "CE") echo "selected"; ?>>CE</option>
                    <option value="CH" <?php if($major == "CH") echo "selected"; ?>>CH</option>
                    <option value="CK" <?php if($major == "CK") echo "selected"; ?>>CK</option>
                    <option value="CO" <?php if($major == "CO") echo "selected"; ?>>CO</option>
                    <option value="CS" <?php if($major == "CS") echo "selected"; ?>>CS</option>
                    <option value="EC" <?php if($major == "EC") echo "selected"; ?>>EC</option>
                    <option value="EE" <?php if($major == "EE") echo "selected"; ?>>EE</option>
                    <option value="EL" <?php if($major == "EL") echo "selected"; ?>>EL</option>
                    <option value="EN" <?php if($major == "EN") echo "selected"; ?>>EN</option>
                    <option value="FT" <?php if($major == "FT") echo "selected"; ?>>FT</option>
                    <option value="GS" <?php if($major == "GS") echo "selected"; ?>>GS</option>
                    <option value="HM" <?php if($major == "HM") echo "selected"; ?>>HM</option>
                    <option value="HU" <?php if($major == "HU") echo "selected"; ?>>HU</option>
                    <option value="ID" <?php if($major == "ID") echo "selected"; ?>>ID</option>
                    <option value="IM" <?php if($major == "IM") echo "selected"; ?>>IM</option>
                    <option value="IT" <?php if($major == "IT") echo "selected"; ?>>IT</option>
                    <option value="MA" <?php if($major == "MA") echo "selected"; ?>>MA</option>
                    <option value="MC" <?php if($major == "MC") echo "selected"; ?>>MC</option>
                    <option value="ME" <?php if($major == "ME") echo "selected"; ?>>ME</option>
                    <option value="MF" <?php if($major == "MF") echo "selected"; ?>>MF</option>
                    <option value="MH" <?php if($major == "MH") echo "selected"; ?>>MH</option>
                    <option value="MK" <?php if($major == "MK") echo "selected"; ?>>MK</option>
                    <option value="MT" <?php if($major == "MT") echo "selected"; ?>>MT</option>
                    <option value="PE" <?php if($major == "PE") echo "selected"; ?>>PE</option>
                    <option value="PO" <?php if($major == "PO") echo "selected"; ?>>PO</option>
                    <option value="PS" <?php if($major == "PS") echo "selected"; ?>>PS</option>
                    <option value="SP" <?php if($major == "SP") echo "selected"; ?>>SP</option>
                    <option value="SS" <?php if($major == "SS") echo "selected"; ?>>SS</option>
                    <option value="ST" <?php if($major == "ST") echo "selected"; ?>>ST</option>
                    <option value="SU" <?php if($major == "SU") echo "selected"; ?>>SU</option>
                    <option value="TM" <?php if($major == "TM") echo "selected"; ?>>TM</option>
                    <option value="XE" <?php if($major == "XE") echo "selected"; ?>>XE</option>
                </select>
            </div>
        </div> 
        <div class="w2ui-field">
            <label>Year:</label>
            <div>
                <select name="year">
                    <option value="1" <?php if($year == "1") echo "selected"; ?>>1</option>
                    <option value="2" <?php if($year == "2") echo "selected"; ?>>2</option>
                    <option value="3" <?php if($year == "3") echo "selected"; ?>>3</option>
                    <option value="4" <?php if($year == "4") echo "selected"; ?>>4</option>
                    <option value="5" <?php if($year == "5") echo "selected"; ?>>5</option>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Section:</label>
            <div><input type="text" name="section" maxlength="5" value="<?php echo $section; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Address:</label>
            <div><input type="text" name="address" maxlength="500" value="<?php echo $address; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Telno:</label>
            <div><input type="text" name="telno" maxlength="30" value="<?php echo $telno; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Parent:</label>
            <div><input type="text" name="parent" maxlength="30" value="<?php echo $parent; ?>" /></div>
        </div>
        <div class="w2ui-field">
            <label>Gender:</label>
            <div>
                <select name="gender">
                    <option value="1" <?php if($gender == "1") echo "selected"; ?>>Male</option>
                    <option value="0" <?php if($gender == "0") echo "selected"; ?>>Female</option>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>College:</label>
            <div>
                <select name="coll">
                	<option value="AS" <?php if($coll == "AS") echo "selected"; ?>>CAS</option>
                    <option value="BE" <?php if($coll == "BE") echo "selected"; ?>>EDU</option>
                    <option value="BN" <?php if($coll == "BN") echo "selected"; ?>>BSN</option>
                    <option value="CO" <?php if($coll == "CO") echo "selected"; ?>>CBA</option>
                    <option value="EN" <?php if($coll == "EN") echo "selected"; ?>>ENG</option>
                    <option value="LAW" <?php if($coll == "LAW") echo "selected"; ?>>LAW</option>
                    <option value="BL" <?php if($coll == "BL") echo "selected"; ?>>LAW</option>
                    <option value="MA" <?php if($coll == "MA") echo "selected"; ?>>MA</option>
                    <option value="MA" <?php if($coll == "MA") echo "selected"; ?>>MA</option>
                    <option value="MD" <?php if($coll == "MD") echo "selected"; ?>>MED</option>
                    <option value="XX" <?php if($coll == "XX") echo "selected"; ?>>XX</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>
        <?php if($row_id == "") { ?>
            <button type="submit" class="btn btn-green" name="insert_user_info">Save</button>            
        <?php } else { ?>
            <button type="submit" class="btn btn-green" name="update_user_info">Save</button>
        <?php } ?>
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form').w2form({ 
        name  : 'form',        
        fields: [
            { field: 'idnumber', required: true },
            { field: 'lname', required: true },
            { field: 'fname', required: true },
            { field: 'mname', required: true },
        ]
    });
});
</script>


