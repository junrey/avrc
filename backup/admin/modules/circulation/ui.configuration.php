<?php

if(isset($_POST["update_config_enrollment"])) {
	$ay   = $_POST['ay'];
	$sem  = $_POST['sem'];
	$term = $_POST['term'];

	mysql_query("UPDATE config SET ay = '$ay', sem = '$sem', term = '$term', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW()") or die(mysql_error());		
}

if(isset($_POST["update_config_circulation"])) {
	$receipt_printing = $_POST['receipt_printing'];
	$receipt_header   = escapeString($_POST['receipt_header']);

	mysql_query("UPDATE config SET receipt_printing = '$receipt_printing', receipt_header='$receipt_header', last_modified_by = '".getSessionVar('usercode')."', last_modified_date = NOW()") or die(mysql_error());		
}


$q2 = mysql_query("SELECT * FROM config") or die(mysql_error());  
$r2 = mysql_fetch_assoc($q2);	

$cay  = date("Y");
$cay2 = date("y") + 1;
$xcay = $cay . "-" . $cay2;

$oay  = date("Y") - 1;
$oay2 = date("y");
$xoay = $oay . "-" . $oay2;

?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript" src="plugin/nicedit/nicEdit.js"></script>
<div id="form1" style="width:500px;">
<form name="form1" action="" method="post">
    <div class="w2ui-page page-0">
        <div class="w2ui-field">
            <label>Academic Year:</label>
            <div>
                <select name="ay">
                    <option value="<?php echo $xoay;?>" <?php if($r2['ay'] == $xoay) { ?>selected<?php } ?>><?php echo $xoay;?></option>
                    <option value="<?php echo $xcay;?>" <?php if($r2['ay'] == $xcay) { ?>selected<?php } ?>><?php echo $xcay;?></option>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Semester:</label>
            <div>
                <select name="sem">
                    <option value="1" <?php if($r2['sem'] == "1") { ?>selected<?php } ?>>1st</option>
                    <option value="2" <?php if($r2['sem'] == "2") { ?>selected<?php } ?>>2nd</option>
                    <option value="3" <?php if($r2['sem'] == "3") { ?>selected<?php } ?>>Summer</option>
                </select>   
            </div>
        </div>
        <div class="w2ui-field">
            <label>Term:</label>
            <div>
                <select name="term">
                    <option value="1" <?php if($r2['term'] == "1") { ?>selected<?php } ?>>Prelim</option>
                    <option value="2" <?php if($r2['term'] == "2") { ?>selected<?php } ?>>Midterm</option>
                    <option value="3" <?php if($r2['term'] == "3") { ?>selected<?php } ?>>Endterm</option>
                </select>
            </div>
        </div>
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>        
        <button type="submit" class="btn btn-green" name="update_config_enrollment">Save</button>               
    </div>
</form>
</div>
<br />
<div id="form2" style="width:500px;">
<form name="form2" action="" method="post">
    <input type="hidden" name="code" value="<?php echo $code; ?>">
    <div class="w2ui-page page-0">
        <div class="w2ui-field" style="width:200px;">
            <label>Receipt Printing:</label>
            <div>
                <select name="receipt_printing">
                    <option value="1" <?php if($r2['receipt_printing'] == 1) { ?>selected<?php } ?>>Yes</option>
                    <option value="0" <?php if($r2['receipt_printing'] == 0) { ?>selected<?php } ?>>No</option>
                </select>
            </div>
        </div>
        <div class="w2ui-field">
            <label>Receipt Header:</label>
            <div><textarea name="receipt_header" id="receipt_header" cols="40" rows="5" style="text-align:center; background-color:#FFFFFF;"><?php echo $r2['receipt_header']; ?></textarea></div>
        </div>     
    </div>
    <div class="w2ui-buttons">
        <button type="reset" class="btn" name="reset">Reset</button>        
        <button type="submit" class="btn btn-green" name="update_config_circulation" onclick="javascript:nicEditors.findEditor('receipt_header').saveContent();">Save</button>               
    </div>
</form>
</div>

<script type="text/javascript">
$(function () {
    $('#form1').w2form({ 
        name  : 'form1',
        header : 'Enrollment Configuration',
        fields: [
         
        ]
    });
    
     $('#form2').w2form({ 
        name  : 'form2',
        header : 'Circulation',
        fields: [
         
        ]
    });
    
    bkLib.onDomLoaded(function() {
        new nicEditor({maxHeight:510,buttonList:['save','bold','italic','underline','left','center','right','justify']}).panelInstance('receipt_header');
		
	});
});
</script>


