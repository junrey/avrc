<div id="grid" style="width:100%; height:100%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/circulation/json.borrowing.item.unreturn.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
			},
			toolbar: {
	            items: [
	                { type: 'button', id: 'excel_form', caption: 'Excel Form', img: 'icon-page' }
	            ],
	            onClick: function (target, data) {
	                if (target == 'excel_form') {
	                    document.location.href='<?php echo $path; ?>/modules/report/xls.report.circulation.borrowing.item.unreturn.php';

	                }
	            }
	        },
			columns: [

				{ field: 'borrow_date', caption: 'Borrow Date', size: '18%' },
				{ field: 'borrower_id', caption: 'ID Number', size: '8%' },
				{ field: 'borrower_name', caption: 'Borrower\'s Name', size: '20%' },
				{ field: 'teacher', caption: 'Teacher', size: '10%' },
				{ field: 'subject', caption: 'Subject', size: '10%' },
				{ field: 'remarks', caption: 'Remarks', size: '10%' },
				{ field: 'barcode', caption: 'Barcode', size: '10%' },
				{ field: 'item', caption: 'Item', size: '10%' },
				{ field: 'venue', caption: 'Venue', size: '10%' },
				{ field: 'staff', caption: 'Served By', size: '10%' },

			],
			multiSearch: false,
			searches: [
				{ field: 'borrower_id', caption: 'ID Number', type: 'text' },
			]
		});
	});
</script>
