<div id="toolbar" style="padding: 4px; border:1px solid silver;"></div>
<div id="grid" style="width:100%; height:90%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">

	$(function () {
		$('#toolbar').w2toolbar({
			name : 'toolbar',
		 	items: [
				{ type: 'html', html: 'Date: <input type=\"text\" name=\"daily\" id=\"daily\">' },
				{ type: 'button', id: 'load', caption: 'Display', img: 'icon-page' },
				{ type: 'button', id: 'excel_form', caption: 'Excel Form', img: 'icon-page' },
			],
			onClick: function (target, data) {
				if (target == 'load') {
					w2ui['grid'].load( '<?php echo $path; ?>/modules/circulation/json.borrowing.logs.daily.php?datenow=' + document.getElementById('daily').value);
				} else if (target == 'excel_form') {
					document.location.href='<?php echo $path; ?>/modules/report/xls.report.borrowing.logs.daily.php?datenow=' + document.getElementById('daily').value;

				}
			}
		});
		
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/circulation/json.borrowing.logs.daily.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
			},
			columns: [

				{ field: 'borrow_date', caption: 'Borrowed Date', size: '18%' },
				{ field: 'borrower_id', caption: 'ID Number', size: '8%' },
				{ field: 'borrower_name', caption: 'Borrower\'s Name', size: '20%' },
				{ field: 'teacher', caption: 'Teacher', size: '10%' },
				{ field: 'subject', caption: 'Subject', size: '10%' },
				{ field: 'remarks', caption: 'Remarks', size: '10%' },
				{ field: 'barcode', caption: 'Barcode', size: '10%' },
				{ field: 'item', caption: 'Item', size: '10%' },
				{ field: 'venue', caption: 'Venue', size: '10%' },
				{ field: 'staff', caption: 'Served By', size: '10%' },
				{ field: 'return_status', caption: 'Return Status', size: '10%' },
				{ field: 'return_date', caption: 'Date Returned', size: '10%' },
				{ field: 'return_duty', caption: 'Received By', size: '10%' },

			],
			multiSearch: false,
			searches: [
				{ field: 'borrower_id', caption: 'ID Number', type: 'text' },
			]
		});
		
		$('#daily').w2field('date');
		
	});
</script>
