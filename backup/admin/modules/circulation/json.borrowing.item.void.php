<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");

if(isset($_GET['trans_id'])) {
	$trans_id = escapeString($_GET['trans_id']);

	try {
		begin();

		mysql_query("UPDATE borrow_header SET postvoid = '1', postvoid_date = NOW(), postvoid_by = '".getSessionVar('usercode')."' WHERE code = '$trans_id'") or die(mysql_error());
		mysql_query("UPDATE borrow_item SET void = '1', void_date = NOW() WHERE void = '0' AND borrow_header = '$trans_id'") or die(mysql_error());

		commit();

	} catch(Exception $e) {
		rollback();
		exit();
	}

}



$limit = "100";
if(isset($_POST['limit'])){
    $limit = escapeString($_POST['limit']);
}

$offset = "0";
if(isset($_POST['offset'])){
    $offset = escapeString($_POST['offset']);
}

$search_field = "";
if(isset($_POST['search'][0]['field'])){
    $search_field = escapeString($_POST['search'][0]['field']);
}

$search_value = "";
if (isset($_POST['search'][0]['value'])){
    $search_value = escapeString($_POST['search'][0]['value']);
}

setUTF8();

$q  = mysql_query("SELECT SQL_CALC_FOUND_ROWS * FROM borrow_header WHERE (CONCAT(code, borrower) LIKE '%$search_value%') ORDER BY borrow_date DESC LIMIT $offset, $limit") or die(mysql_error());
$q2 = mysql_query("SELECT FOUND_ROWS() AS total") or die(mysql_error());
$r2 = mysql_fetch_assoc($q2);

$total_records = $r2['total'];
?>

<?php

echo "{ \"total\": $total_records, \"records\": [";
	$cnt = 0;
	while($r = mysql_fetch_assoc($q)) {
		$cnt++;
		if($cnt > 1) {
			echo ",";
		}
		
		$borrower = "";
		$q2 = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$r[borrower]'");
		if(mysql_num_rows($q2) > 0) {
			$r2 = mysql_fetch_assoc($q2);
			$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

		} else {
			$q2 = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$r[borrower]'");
			if(mysql_num_rows($q2) > 0) {
				$r2 = mysql_fetch_assoc($q2);
				$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

			} else {
				$borrower = $r['borrower'];
			}

		}

		if($r['postvoid']) {
			$void  = "Yes";
			$style = "background-color: #FF9999";
		} else {
			$void  = "No";
			$style = "";
		}

       	echo "{ \"recid\": \"$r[code]\", \"trans_id\": \"$r[code]\", \"staff\": \"" . cleanString(getUserLogName($r['usercode'])) . "\", \"borrower_id\": \"$r[borrower]\", \"borrower\": \"" . cleanString($borrower) . "\", \"borrow_date\": \"" . datetime("m/j/y", $r['borrow_date']) . " " . datetime("h:i:s a", $r['borrow_date']) . "\", \"void\": \"$void\", \"style\": \"$style\" }";
   }

echo " ] }";

?>
