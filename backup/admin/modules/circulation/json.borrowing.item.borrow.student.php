<?php session_start();

require_once("../../../db/db.connection.php");
require_once("../../../inc/inc.functions.php");


$limit = "100";
if(isset($_POST['limit'])){
    $limit = escapeString($_POST['limit']);
}

$offset = "0";
if(isset($_POST['offset'])){
    $offset = escapeString($_POST['offset']);
}

$coll = "ALL";
if(isset($_POST['coll'])){
    $coll = escapeString($_POST['coll']);
}

$sem = getSessionVar('sem');
if(isset($_POST['sem'])){
    $sem = escapeString($_POST['sem']);
}

$ay = getSessionVar('ay');
if(isset($_POST['ay'])){
    $ay = escapeString($_POST['ay']);
}

setUTF8();

if($coll == "ALL") {
	$q = mysql_query("SELECT SQL_CALC_FOUND_ROWS a.*, b.ay, b.coll, CASE b.sem WHEN '1' THEN '1st' WHEN '2' THEN '2nd' WHEN '3' THEN 'Summer' END AS sem FROM borrow_item AS a INNER JOIN borrow_header AS b ON a.borrow_header = b.code WHERE a.borrow_header <> '' AND a.void = '0' AND b.sem = '$sem' AND b.ay = '$ay' AND b.isstudent = '1' ORDER BY a.borrow_date DESC LIMIT $offset, $limit") or die(mysql_error());
} else {
	$q = mysql_query("SELECT SQL_CALC_FOUND_ROWS a.*, b.ay, b.coll, CASE b.sem WHEN '1' THEN '1st' WHEN '2' THEN '2nd' WHEN '3' THEN 'Summer' END AS sem FROM borrow_item AS a INNER JOIN borrow_header AS b ON a.borrow_header = b.code WHERE a.borrow_header <> '' AND a.void = '0' AND b.sem = '$sem' AND b.ay = '$ay' AND b.isstudent = '1' AND b.coll = '$coll' ORDER BY a.borrow_date DESC LIMIT $offset, $limit") or die(mysql_error());
}

$q2 = mysql_query("SELECT FOUND_ROWS() AS total") or die(mysql_error());
$r2 = mysql_fetch_assoc($q2);

$total_records = $r2['total'];
?>

<?php

echo "{ \"total\": $total_records, \"records\": [";
	$cnt = 0;
	while($r = mysql_fetch_assoc($q)) {	
		$cnt++;
		if($cnt > 1) {
			echo ",";
		}
		
		$q3 = mysql_query("SELECT c.lname, c.fname FROM student AS c WHERE c.idnumber = '$r[borrower]' LIMIT 1") or die(mysql_error());
		$r3 = mysql_fetch_assoc($q3);
	
		$borrower = strtoupper($r3['lname']) . ", " .strtoupper($r3['fname']);

		$q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
		$r2 = mysql_fetch_assoc($q2);

		$status    = "Yes";
		$status_bg = "";

		if($r['item_return'] == "0") {
			$status    = "No";
			$status_bg = "background-color: #FF3333";
		}
		
		if($r['a_return_date'] == "0000-00-00 00:00:00") {
			$return_date = "";	
		} else {
			$return_date = datetime("m/j/y h:i:s a", $r['a_return_date']);
		}
		
		echo "{ \"recid\": \"$r[itemcode]\", \"borrow_date\": \"" . datetime("m/j/y h:i:s a", $r['borrow_date']) . "\", \"borrower_id\": \"$r[borrower]\", \"borrower_name\": \"" . cleanString($borrower) . "\", \"teacher\": \"" . cleanString($r['teacher']) . "\", \"subject\": \"" . cleanString($r['subject']) . "\", \"remarks\": \"" . cleanString($r['remarks']) . "\", \"barcode\": \"$r[barcode]\", \"item\": \"" . cleanString($r2['description']) . "\", \"venue\": \"" . cleanString($r['venue']) . "\", \"borrow_duty\": \"" . cleanString(getUserLogName($r['usercode'])) . "\", \"return_date\": \"$return_date\", \"return_duty\": \"" . cleanString(getUserLogName($r['rusercode'])) . "\", \"status\": \"$status\", \"style\": \"$status_bg\", \"college\": \"$r[coll]\", \"sem\": \"$r[sem]\", \"ay\": \"$r[ay]\" }";
	
   }

echo " ] }";

?>
