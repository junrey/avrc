<div id="toolbar" style="padding: 4px; border:1px solid silver;"></div>
<div id="grid" style="width:100%; height:90%;"></div>

<?php
	$path = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$path .=$_SERVER["SERVER_NAME"] . dirname($_SERVER["PHP_SELF"]);
?>
<?php include_once("index.header.php"); ?>
<script type="text/javascript">
	$(function () {
		$('#toolbar').w2toolbar({
			name : 'toolbar',
			items: [
				{ type: 'html', html: 'College: <select name="coll" id="coll"><option value="ALL">ALL</option><option value="BSN">BSN</option><option value="CAS">CAS</option><option value="CBA">CBA</option><option value="EDU">EDU</option><option value="ENG">ENG</option><option value="LAW">LAW</option><option value="MED">MED</option><option value="OTHR">OTHR</option></select>' },
				{ type: 'break' },
				{ type: 'html', html: 'Semester: <select name="sem" id="sem"><option value="1">1st</option><option value="2">2nd</option><option value="3">Summer</option></select>' },
				{ type: 'break' },
			
				<?php $q = mysql_query("SELECT DISTINCT ay FROM borrow_header WHERE ay <> ''") or die(mysql_error()); ?>
			
				{ type: 'html', html: 'Academic Year: <select name="ay" id="ay"><?php while ($r = mysql_fetch_array($q)) { ?><option value="<?php echo $r['ay']; ?>"><?php echo $r['ay']; ?></option><?php } ?></select>' },
				{ type: 'break' },
				{ type: 'button', id: 'display', caption: 'Display', img: 'icon-page' },
				{ type: 'button', id: 'excel_form', caption: 'Excel Form', img: 'icon-page' },
			],
			onClick: function (target, data) {
				if(target == 'excel_form') {
					document.location.href='<?php echo $path; ?>/modules/report/xls.report.circulation.borrowing.item.borrow.student.php?coll=' + $('#coll').val() + '&sem=' + $('#sem').val() + '&ay=' + $('#ay').val();
			
				} else if(target == 'display') { 
					w2ui['grid'].postData['coll'] = $('#coll').val();
					w2ui['grid'].postData['sem'] = $('#sem').val();
					w2ui['grid'].postData['ay'] = $('#ay').val();
					w2ui['grid'].load( '<?php echo $path; ?>/modules/circulation/json.borrowing.item.borrow.student.php');
				}
			}
		});
		$('#grid').w2grid({
			name: 'grid',
			header: '<?php echo $p_menu_name; ?>',
            multiSelect : false,
			url: '<?php echo $path; ?>/modules/circulation/json.borrowing.item.borrow.student.php',
			show: {
				header        : true,
				toolbar       : true,
				footer        : true,
				lineNumbers   : true,
				toolbarSearch : false,
			},
			columns: [

				{ field: 'borrow_date', caption: 'Borrowed Date', size: '10%' },
				{ field: 'borrower_id', caption: 'ID Number', size: '8%' },
				{ field: 'borrower_name', caption: 'Borrower\'s Name', size: '15%' },
				{ field: 'college', caption: 'College', size: '10%' },
				{ field: 'teacher', caption: 'Teacher', size: '10%' },
				{ field: 'subject', caption: 'Subject', size: '10%' },
				{ field: 'remarks', caption: 'Remarks', size: '10%' },
				{ field: 'barcode', caption: 'Barcode', size: '10%' },
				{ field: 'item', caption: 'Item', size: '10%' },
				{ field: 'venue', caption: 'Venue', size: '10%' },
				{ field: 'borrow_duty', caption: 'Served By', size: '10%' },
				{ field: 'status', caption: 'Return Status', size: '10%' },
				{ field: 'return_date', caption: 'Date Returned', size: '10%' },
				{ field: 'return_duty', caption: 'Received By', size: '10%' },
				{ field: 'sem', caption: 'Semester', size: '10%' },
				{ field: 'ay', caption: 'Academic Year', size: '10%' },

			],
			postData: {
				coll: $('#coll').val(), 
				sem: $('#sem').val(),
				ay: $('#ay').val(),
			},
		});
	});
</script>
