<?php session_start();
    require_once("../db/db.connection.php");
    require_once("../inc/inc.functions.php");

    if(!isset($_SESSION[getSystemName()]['usercode'])) {
        exit();
    }


    if(isset($_GET['p']) && trim($_GET['p']) != "") {
        $p_mneu = trim($_GET['p']);

        $q = mysql_query("SELECT code, keyword, filename, menu_name, ispopup, width, height FROM _program WHERE code = '$p_mneu' OR keyword = '$p_mneu'") or die(mysql_error());
        $r = mysql_fetch_assoc($q);

        $p_menu_code     = $r['code'];
        $p_menu_keyword  = $r['keyword'];
        $p_menu_filename = $r['filename'];
        $p_menu_name     = $r['menu_name'];
        $p_menu_ispopup  = $r['ispopup'];
        $p_menu_width    = $r['width'];
        $p_menu_height   = $r['height'];

        if($p_menu_filename != "") {
            if(getSessionVar('usercode') == getRootCode()) {
                include ("$p_menu_filename");
            } else {
                // checking if rights
                $q = mysql_query("SELECT program_code FROM _userprogram WHERE program_code = '$p_menu_code' AND usercode = '".getSessionVar('usercode')."'") or die(mysql_error());
                if(mysql_num_rows($q) > 0) {
                    include ("$p_menu_filename");
                } else {
                    echo "<script>parent.w2alert('You have no rights for this module!','', function () { parent.location.reload(); });</script>";
                }
            }
			
			if(isset($form_submit_status)) {
				if($form_submit_status == 2) {
			    	echo "<script>parent.w2alert('Duplicate Entry!','', function () { parent.location.reload(); });</script>";
				}
			}
        }
    }

?>
