// function key used
// f7,f8,f10,cancel,enter


// 1 - enter (borrow item)
// 2 - f8 (void item)
// 3

var fkey          = 1;
var upx           = 1;
var borrower      = "";
var datenow       = "";
var return_hour   = "";
var return_minute = "";
var venue         = "";
var subject       = "";
var remarks       = "";


/*function setFunction() {
	if(fkey == 3) returnItemEnter();

}
*/
//////////////////////////////////////////////
function setEnter() {
	shortcut.add("Enter", function() {
		addItem();
	});
}
function addItem() {
	if(borrower == "") {
		$('#barcode').val("");
	} else {
		var barcode = $.trim($('#barcode').val());
		if(barcode == "") {
			return;
		} else {
			$('#item').load("borrowing.process.php?barcode=" + barcode);
			$('#barcode').val("").focus();
		}
	}
}
/////////////////////////////////////////////
function setSpace() {
	shortcut.add("Space", function() {
		newTrans();
	});
}
function newTrans() {
	setOnlyFKey("Space");
	$('#besc').attr("onClick", "newTransCancel()");
	shortcut.remove("Esc");
	shortcut.add("Esc", function() {
		newTransCancel();

	});
	barcodeDisSetup();
	$('#borrower').val("").focus();
	$('#window_newTrans').dialog('open');
	$('#search_borrower').empty();
	borrower = "";
	clearscreen();
	shortcut.remove("Enter");
	shortcut.add("Enter", function() {
		newTransEnter();
	});
}
function newTransCancel() {
	$('#besc').attr("onClick", "");
	shortcut.remove("Esc");
	shortcut.remove("Enter");
	borrower = "";
	clearscreen();
	$('#window_newTrans').dialog('close');
	barcodeSetup();
	setDefaultFKey();
}
function newTransEnter() {
	var btemp = $.trim($('#borrower').val());
	if(btemp) {
		borrower = btemp.replace('-','');
		if($.isNumeric(borrower)) {
			$('#item').load("borrowing.item.return.php?borrower=" + borrower);
			$('#borrower_name').load("borrowing.new.php?borrower=" + borrower);
			$('#window_newTrans').dialog('close');
			barcodeSetup();
			setEnableFKey("F7");
			setEnableFKey("F8");
			setEnableFKey("F10");
			shortcut.remove("Enter");
			setEnter();
		} else {
			$('#borrower').select();
		}
	}
}
function searchBorrower() {
	var borrower = $.trim($('#borrower').val());
	if(borrower) {
		$('#search_borrower').load("borrowing.borrower.search.php?borrower=" + borrower);
	}
}

/////////////////////////////////////////////
function setF1() {
	shortcut.add("F1", function() {
		reprint();
	});
}
function reprint() {
	setOnlyFKey("F1");
	$('#besc').attr("onClick", "reprintCancel()");
	shortcut.remove("Esc");
	shortcut.add("Esc", function() {
		reprintCancel();
	});
	barcodeDisSetup();
	$('#window_reprint').dialog('open');
	$('#reprint_trans_id').val("").focus();
	$('#reprint_username').val("");
	$('#reprint_date').val("");
	$('#search_reprint').html("");
}
function reprintSearch() {
	var reprint_trans_id = $.trim($('#reprint_trans_id').val());
	var reprint_username = $.trim($('#reprint_username').val());
	var reprint_date     = $.trim($('#reprint_date').val());

	if(reprint_trans_id == "" && reprint_username == "" && reprint_date == "") {
		return;
	} else {
		$('#search_reprint').load("borrowing.item.reprint.php?trans_id=" + reprint_trans_id + "&trans_username=" + reprint_username + "&trans_date=" + reprint_date);
		$('#reprint_trans_id').val("").focus();
		$('#reprint_username').val("");
		$('#reprint_date').val("");
	}
}
function reprintCancel() {
	$('#besc').attr("onClick", "");
	shortcut.remove("Esc");
	barcodeSetup();
	setDefaultFKey();
	$('#window_reprint').dialog('close');
}

/////////////////////////////////////////////
function setF7() {
	shortcut.add("F7", function() {
		voidAll();
	});
}
function voidAll() {
	if(parseInt(w2ui['item'].total) > 0) {
		setOnlyFKey("F7");
		barcodeDisSetup();
		if(confirm("Do you want to void all item(s)?")) {
			$('#item').load("borrowing.process.php?void_all=1");
			$('#barcode').val("");
		}
		barcodeSetup();
		setEnableFKey("Space");
		setEnableFKey("F10");
		setEnableFKey("F8");
	} else {
		$('#barcode').val("").focus();
	}
}

/////////////////////////////////////////////
function setF8() {
	shortcut.add("F8", function() {
		voidItem();
	});
}
function voidItem() {
	if(parseInt(w2ui['item'].total) > 0) {
		setOnlyFKey("F8");
		$('#besc').attr("onClick", "voidItemCancel()");
		shortcut.remove("Esc");
		shortcut.add("Esc", function() {
			voidItemCancel();
		});
		barcodeHotSetup();
		shortcut.remove("Enter");
		shortcut.add("Enter", function() {
			voidItemEnter();
		});
	} else {
		$('#barcode').val("").focus();
	}
}
function voidItemEnter() {
	var barcode = $.trim($('#barcode').val());
	$('#item').load("borrowing.process.php?void_barcode=" + barcode);
	barcodeHotSetup();
}
function voidItemCancel() {
	$('#besc').attr("onClick", "newTransCancel()");
	shortcut.remove("Esc");
	shortcut.add("Esc", function() {
		newTransCancel();
	});
	barcodeSetup();
	setEnableFKey("Space");
	setEnableFKey("F10");
	setEnableFKey("F7");
	shortcut.remove("Enter");
	setEnter();
}

/////////////////////////////////////////////
function setF10() {
	shortcut.add("F10", function() {
		tender();
	});
}
function tender() {
	if(parseInt(w2ui['item'].total) > 0) {
		setOnlyFKey("F10");
		$('#besc').attr("onClick", "tenderCancel()");
		shortcut.remove("Esc");
		shortcut.add("Esc", function() {
			 tenderCancel();
		});
		shortcut.remove("Enter");
		barcodeDisSetup();
		$('#tender_panel').css('visibility', 'visible');
		upx = 1;

		$('#datenow').datepicker().datepicker('setDate',new Date());

		$('#venue').val("").select();
		$('#subject').val("none");
		$('#tborrower').val("");
		$('#remarks').val("");

		shortcut.remove("Enter");
		shortcut.add("Enter", function() {
			tenderEnter();
		});
	} else {
		$('#barcode').val("").focus();
	}
}
function tenderCancel() {
	$('#besc').attr("onClick", "newTransCancel()");
	shortcut.remove("Esc");
	shortcut.add("Esc", function() {
		newTransCancel();
	});
	barcodeSetup();
	$('#tender_panel').css('visibility', 'hidden');
	setEnableFKey("Space");
	setEnableFKey("F7");
	setEnableFKey("F8");
	shortcut.remove("Enter");
	setEnter();

}
function tenderEnter() {
	datenow       = $('#datenow').val();
	venue         = $.trim($('#venue').val());
	subject       = $.trim($('#subject').val());
	tborrower     = $.trim($('#tborrower').val());
	remarks       = $.trim($('#remarks').val());

	if(datenow == "" || datenow == 0) return;
	else if(venue == "") return;
	else if(subject == "") return;

	$('#besc').attr("onClick", "");
	shortcut.remove("Esc");
	$('#tender_panel').css('visibility', 'hidden');
	fkey = 1;
	setDefaultFKey();
	shortcut.remove("Enter");

	/*$.post("borrowing.item.tender.php", { return_date: datenow, venue: venue, subject: subject, borrower: borrower, teacher: tborrower, remarks: remarks })
	.done(function(data) {
		$('#print_panel').html(data);		
	});*/
	
	
	var param   = "return_date=" + datenow + "&venue=" + venue + "&subject=" + subject + "&borrower=" + borrower + "&teacher=" + tborrower + "&remarks=" + remarks;
	
	if(document.getElementById('receipt_printing').value == "1") {
		getPostPagePrint('print_panel', 'borrowing.item.tender.php', 'window_process_loader', param, 2);
	} else {
		getPostPage('print_panel', 'borrowing.item.tender.php', 'window_process_loader', param, 2);
	}	
	
	

	barcodeDisSetup();
	borrower = "";
	clearscreen();
}
function upxAlter(newVal) {
	upx = newVal;
}
function arrowUp() {
	shortcut.add("up", function() {
		if(upx == 4) {
			$('#tborrower').select();
		} else if(upx == 3) {
			$('#subject').select();
		} else if(upx == 2) {
			$('#venue').select();
		}
	});
}
function arrowDown() {
	shortcut.add("down", function() {
		if(upx == 1) {
			$('#subject').select();
		} else if(upx == 2) {
			$('#tborrower').select();
		} else if(upx == 3) {
			$('#remarks').select();
		}
	});
}

/////////////////////////////////////////////
function setF12() {
	shortcut.add("F12", function() {
		returnItem();
	});
}
function returnItem() {
	setOnlyFKey("F12");
	$('#besc').attr("onClick", "returnItemEnterCancel()");
	shortcut.remove("Esc");
	shortcut.add("Esc", function() {
		returnItemEnterCancel();
	});

	barcodeHotSetup();
	shortcut.remove("Enter");
	shortcut.add("Enter", function() {
		returnItemEnter();
	});
}
function returnItemEnter() {
	var barcode = $.trim($('#barcode').val());
	$('#item').load("borrowing.item.return.process.php?return_barcode=" + barcode);
	barcodeHotSetup();
}
function returnItemEnterCancel() {
	$('#besc').attr("onClick", "");
	shortcut.remove("Esc");
	shortcut.remove("Enter");
	barcodeSetup();
	setDefaultFKey();
}
/////////////////////////////////////////////
function setOnlyFKey(setkey) {
	if(setkey != "Space") { shortcut.remove("Space"); $('#bspace').addClass('ui-state-disabled').prop('disabled', true); }
	if(setkey != "F1") {    shortcut.remove("F1");    $('#bf1').addClass('ui-state-disabled').prop('disabled', true); }
	if(setkey != "F7") {    shortcut.remove("F7");    $('#bf7').addClass('ui-state-disabled').prop('disabled', true); }
	if(setkey != "F8") {    shortcut.remove("F8");    $('#bf8').addClass('ui-state-disabled').prop('disabled', true); }
	if(setkey != "F10") {   shortcut.remove("F10");   $('#bf10').addClass('ui-state-disabled').prop('disabled', true); }
	if(setkey != "F12") {   shortcut.remove("F12");   $('#bf12').addClass('ui-state-disabled').prop('disabled', true); }
}
function setEnableFKey(setkey) {
	if(setkey == "Space") {    setSpace(); $('#bspace').removeClass('ui-state-disabled').prop('disabled', false); }
	else if(setkey == "F1") {  setF1();    $('#bf1').removeClass('ui-state-disabled').prop('disabled', false); }
	else if(setkey == "F7") {  setF7();    $('#bf7').removeClass('ui-state-disabled').prop('disabled', false); }
	else if(setkey == "F8") {  setF8();    $('#bf8').removeClass('ui-state-disabled').prop('disabled', false); }
	else if(setkey == "F10") { setF10();   $('#bf10').removeClass('ui-state-disabled').prop('disabled', false); }
	else if(setkey == "F12") { setF12();   $('#bf12').removeClass('ui-state-disabled').prop('disabled', false); }
}
function setDefaultFKey() {
	shortcut.remove("Space"); setSpace(); $('#bspace').removeClass('ui-state-disabled').prop('disabled', false);
	shortcut.remove("F1");    setF1();    $('#bf1').removeClass('ui-state-disabled').prop('disabled', false);
	shortcut.remove("F7");    setF7();    $('#bf7').addClass('ui-state-disabled').prop('disabled', true);
	shortcut.remove("F8");    setF8();    $('#bf8').addClass('ui-state-disabled').prop('disabled', true);
	shortcut.remove("F10");   setF10();   $('#bf10').addClass('ui-state-disabled').prop('disabled', true);
	shortcut.remove("F12");   setF12();   $('#bf12').removeClass('ui-state-disabled').prop('disabled', false);
}
/////////////////////////////////////////////
function searchItem() {
	shortcut.remove("Esc");
	var barcode = $.trim($('#barcode').val());
	//$('#search_item').load('borrowing.item.search.php?barcode=' + barcode);
	$('#barcode').focus(); $('#window_item_search').load('borrowing.item.search.php').dialog('open');
}
function barcodeSetup() {
	$('#barcode').removeClass('ui-state-disabled ui-state-error').prop('disabled', false).val("").focus();
}
function barcodeHotSetup() {
	$('#barcode').removeClass('ui-state-disabled').addClass('ui-state-error').prop('disabled', false).val("").focus();
}
function barcodeDisSetup() {
	$('#barcode').removeClass('ui-state-error').addClass('ui-state-disabled').prop('disabled', true);
}
function clearscreen() {
	$('#borrower_name').html("Trasaction completed. Press space bar for new.");
	$('#item').html("");
}
/////////////////////////////////////////////
function startup() {
	setDefaultFKey();
	arrowUp();
	arrowDown();
	newTrans();
	$('#borrower').focus();
}

window.onload = startup;
