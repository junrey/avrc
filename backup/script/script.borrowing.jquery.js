$(function(){
	$("#window_newTrans").dialog({
		autoOpen: false,
		modal: true,
		width: 650,
		height: 350,
		position: 'center',
		draggable: false,
		resizable: false
	}).siblings(".ui-dialog-titlebar").hide();

	$("#window_reprint").dialog({
		autoOpen: false,
		modal: true,
		width: 750,
		height: 350,
		position: 'center',
		draggable: false,
		resizable: false
	}).siblings(".ui-dialog-titlebar").hide();

	$("#window_item_search").dialog({
		autoOpen: false,
		modal: true,
		width: 650,
		height: 400,
		position: 'center',
		draggable: false,
		resizable: false
	}).siblings(".ui-dialog-titlebar").hide();

	jQuery(document).ajaxStart(function() { $().w2popup({ width:200, height:60, modal:true }).lock('Loading', true); }).ajaxStop(function() { w2popup.close(); });
	jQuery.ajaxSetup({async:true});



});
