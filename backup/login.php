<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>System Login</title>
  <link rel="stylesheet" href="plugin/login/css/style.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <script src="plugin/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="plugin/w2ui/w2ui-1.4.2.min.js"></script>
</head>
<body>
  <h1 class="register-title">AVRC</h1>
  <form id="login" method="post" action="login.process.php" class="register">
    <div class="register-switch">
      <input type="radio" name="login" value="USER" id="login_a" class="register-switch-input" checked>
      <label for="login_a" class="register-switch-label">Circulation</label>
      <input type="radio" name="login" value="ADMIN" id="login_b" class="register-switch-input">
      <label for="login_b" class="register-switch-label">Admin Panel</label>
    </div>
    <input type="text" name="username" class="register-input" placeholder="Username" required autocomplete="off">
    <input type="password" name="password" class="register-input" placeholder="Password" required>
    <input type="submit" value="Login" name="submit_form" class="register-button" />
  </form>
  <?php /*
  <div class="about">
    <p class="about-links">
      <a href="http://www.cssflow.com/snippets/registration-form" target="_parent">View Article</a>
      <a href="http://www.cssflow.com/snippets/registration-form.zip" target="_parent">Download</a>
    </p>
    <p class="about-author">
      &copy; 2013 <a href="http://thibaut.me" target="_blank">Thibaut Courouble</a> -
      <a href="http://www.cssflow.com/mit-license" target="_blank">MIT License</a><br>
      Original PSD by <a href="http://dribbble.com/shots/1115108-Register-UI-Free-PSD-included" target="_blank">Ionut Zamfir</a>
    </p>
  </div>
  */ ?>
</body>
</html>
