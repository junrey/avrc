<?php session_start();
require_once("db/db.connection.php");
require_once("inc/inc.functions.php");

if(isset($_POST['submit_form']) && trim($_POST['username']) != "" && trim($_POST['password']) != "") {

	$username = escapeString($_POST['username']);
	$password = escapeString($_POST['password']);

    if($_POST['login'] == "ADMIN") {
        $location = "<script>top.location.href='admin/index.php';</script>";
    } else  {
        $location = "<script>top.location.href='index.php';</script>";
    }

	if($username == 'dbadmin' && $password == 'a') {
		$_SESSION[getSystemName()]['usercode'] = getRootCode();
		$_SESSION[getSystemName()]['username'] = getRootUsername();
	 	$_SESSION[getSystemName()]['name']     = "Administrator";

        // external configuration
		$q = mysql_query("SELECT ay, sem, term FROM $db_avrc.config") or die(mysql_error());
		$r = mysql_fetch_assoc($q);
        $_SESSION[getSystemName()]['ay']   = $r['ay'];
        $_SESSION[getSystemName()]['sem']  = $r['sem'];
        $_SESSION[getSystemName()]['term'] = $r['term'];


        // cleaning
        // query for removing previous scan items
        $q = mysql_query("SELECT itemcode FROM borrow_item WHERE void = '0' AND borrow_header = '' AND DATE(borrow_date) < CURDATE() LIMIT 1") or die(mysql_error());
        if(mysql_num_rows($q) > 0) {
            try {
                begin();
                mysql_query("DELETE FROM borrow_item WHERE void = '0' AND borrow_header = '' AND DATE(borrow_date) < CURDATE()") or die(mysql_error());
                commit();
            } catch(Exception $e) {
                rollback();
                exit();
            }
        }

	 	echo $location;

	} else {

		$password = md5($password);

  		$q = mysql_query("SELECT * FROM _user WHERE username = '$username' AND password = '$password' AND active = '1'");
  		if(mysql_num_rows($q) > 0) {
            $r = mysql_fetch_assoc($q);
            $_SESSION[getSystemName()]['usercode'] = $r['usercode'];
            $_SESSION[getSystemName()]['username'] = $r['username'];
            $_SESSION[getSystemName()]['name']     = $r['lname'] . ", " . $r['fname'];

            mysql_query("INSERT INTO _user_logfile(usercode, username, log_date, status) VALUES('$r[usercode]', '$r[username]', NOW(), 'LOGIN')") or die(mysql_error());
            mysql_query("UPDATE _user SET last_login = NOW() WHERE usercode = username = '$r[usercode]'") or die(mysql_error());

			// external configuration
			$q = mysql_query("SELECT ay, sem, term FROM $db_avrc.config") or die(mysql_error());
			$r = mysql_fetch_assoc($q);
	        $_SESSION[getSystemName()]['ay']   = $r['ay'];
	        $_SESSION[getSystemName()]['sem']  = $r['sem'];
	        $_SESSION[getSystemName()]['term'] = $r['term'];


			// cleaning
            // query for removing previous scan items
            $q = mysql_query("SELECT itemcode FROM borrow_item WHERE void = '0' AND borrow_header = '' AND DATE(borrow_date) < CURDATE() LIMIT 1") or die(mysql_error());
            if(mysql_num_rows($q) > 0) {
                try {
                    begin();
                    mysql_query("DELETE FROM borrow_item WHERE void = '0' AND borrow_header = '' AND DATE(borrow_date) < CURDATE()") or die(mysql_error());
                    commit();
                } catch(Exception $e) {
                    rollback();
                    exit();
                }
            }

    	 	echo $location;

		} else {
			echo "<script>alert('Incorrect username and password!'); history.go(-1);</script>";
		}
	}

} else {
	echo "<script>top.location.href='login.php';</script>";
}

?>
