<?php session_start();
    require_once("db/db.connection.php");
    require_once("inc/inc.functions.php");

    if(!isset($_SESSION[getSystemName()]['usercode'])) {
       header ("location: login.php");
    }

    if(getSessionVar('usercode') != getRootCode()) {
        $q = mysql_query("SELECT a.usercode FROM _userprogram AS a INNER JOIN _program AS b ON a.program_code = b.code WHERE a.usercode = '".getSessionVar('usercode')."' AND b.keyword = 'circulation'") or die(mysql_error());
        if(mysql_num_rows($q) == 0) {
            header ("location: login.php");
        }
    }

?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo getClient(); ?> | <?php echo getSystemName(); ?></title>
<link rel="stylesheet" type="text/css" href="plugin/w2ui/w2ui-1.4.3.min.css" />
<link rel="stylesheet" type="text/css" href="style/style.css" />
<script src="plugin/jquery/js/jquery-2.0.0.min.js"></script>
<script src="plugin/w2ui/w2ui-1.4.3.min.js"></script>
</head>
<body>

<div id="layout" style="position: absolute; width: 100%; height: 100%;"></div>

<div id="window_newTrans" style="display: none; height: 400px; overflow: hidden">
    <div rel="title">Borrower's ID Number</div>
    <div rel="body" style="padding: 10px; line-height: 150%">
        <div class="w2ui-page page-0">
            <div class="w2ui-field">
                <input type="text" name="borrower" id="borrower" required placeholder="Search ID Number, Last Name." style="font-size:18px; height:40px; width:300px;" />
                <button onclick="searchBorrower($('#window_newTrans #borrower').val())" class="f_button b_gray">Search</button>
                <button onclick="newTransEnter();" class="f_button b_dark_green">Enter</button>
            </div>
            <div id="search_borrower"></div>
        </div>

    </div>
</div>

<script>
$(function () {

    $('#layout').w2layout({
        name: 'layout',
        panels: [
            { type: 'top', size: 40, style: 'border: 1px solid #dfdfdf; padding: 5px;', content: 'top' },
            { type: 'main', style: 'border: 1px solid #dfdfdf; overflow: hidden;' }
        ]
    });

    w2ui['layout'].content('top',  $().w2toolbar({
        name: 'toolbar',
        style: 'background-image: linear-gradient(#dae6f3,#c2d5ed);',
        items: [
            { type: 'html', html: '<div style="color:#444; font-weight: bold; margin-left:10px;"><?php echo getSystemName(); ?></div>'},
            { type: 'spacer' },
            { type: 'html', html: 'User: <b><?php echo getSessionVar("name"); ?></b>' },
            { type: 'break'},
            { type: 'html', html: 'Term: <b><?php if(getSessionVar("term") == "1") echo "Prelim"; else if(getSessionVar("term") == "2") echo "Midterm"; else echo "Endterm"; ?></b>' },
            { type: 'break'},
            { type: 'html', html: 'Sem: <b><?php if(getSessionVar("sem") == "1") echo "1st"; else if(getSessionVar("sem") == "2") echo "2nd"; else echo "Summer"; ?></b>' },
            { type: 'break' },
            { type: 'html', html: 'AY: <b><?php echo getSessionVar("ay"); ?></b>' },
            { type: 'break'},
            { type: 'html', html: '<button onclick="document.location.href=\'admin\'" class="btn">Admin</button>' },
            { type: 'html', html: '<button onclick="document.location.href=\'logout.php\'" class="btn btn-red">Logout</button>' },
        ]
    }));

    w2ui['layout'].content('main', '<iframe src="modules/borrowing/borrowing.main.php" class="frame_custom"></iframe>');


});

</script>
</body>
</html>
