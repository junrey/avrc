<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(isset($_GET["trans_id"]) && trim($_GET["trans_id"]) != "" || isset($_GET["trans_date"]) && trim($_GET["trans_date"]) != "" || isset($_GET["trans_username"]) && trim($_GET["trans_username"]) != "") {
	$trans_id       = escapeString($_GET['trans_id']);
	$trans_username = escapeString($_GET['trans_username']);
	$trans_date     = escapeString($_GET['trans_date']);

	setUTF8();

	if($trans_id != "") {
		$q = mysql_query("SELECT code, borrower, borrow_date FROM borrow_header WHERE code = '$trans_id' AND postvoid <> '1'") or die(mysql_error());
	} else if($trans_username != "") {
		$q = mysql_query("SELECT code ,borrower, borrow_date FROM borrow_header WHERE borrower = '$trans_username' AND postvoid <> '1'") or die(mysql_error());
	} else if($trans_date != "") {
		$q = mysql_query("SELECT code, borrower, borrow_date FROM borrow_header WHERE YEAR(borrow_date) = YEAR('$trans_date') AND MONTH(borrow_date) = MONTH('$trans_date') AND DAY(borrow_date) = DAY('$trans_date') AND postvoid <> '1' ORDER BY borrower") or die(mysql_error());
	}

	if(mysql_num_rows($q) > 0) {
	?>

		<script>
			$(function() {
				$().w2destroy("search_reprint");
				$('#search_reprint').w2grid({
					name: 'search_reprint',
					multiSelect : false,
					show : {
						header        : false,
						lineNumbers   : true,
						toolbar       : true,
						toolbarSearch : false,
						toolbarReload : false,
						toolbarColumns: false
					},
					toolbar: {
		                items: [
		                    { type: 'button', id: 'print', caption: 'Print Transaction Receipt', img: 'icon-page', disabled: true }
		                ],
		                onClick: function (target, data) {
		                    if (target == 'print') {
		                        var sel = w2ui['search_reprint'].getSelection();
		                        var recid = sel[0];
								var row = w2ui['search_reprint'].get(sel[0]);
								
								getPagePrint('print_panel', 'borrowing.item.reprint.process.php?code=' + recid + '&borrower=' +  row['idnumber'], 'window_process_loader', 2); 

								//$('#print_panel').load('borrowing.item.reprint.process.php?code=' + recid + '&borrower=' + row['idnumber']);
								$('#window_reprint').dialog('close');
							}
		                }
		            },
					columns: [
						{ field: 'code', caption: 'Transaction ID', size: '20%' },
						{ field: 'idnumber', caption: 'ID Number',  size: '40%'},
						{ field: 'borrower', caption: 'Borrower',  size: '40%'},
						{ field: 'borrow_date', caption: 'Borrow Date', size: '20%' },
					],
					records: [
						<?php while($r = mysql_fetch_assoc($q)) { ?>
							<?php
								$borrower = "";
								$q2 = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$r[borrower]'") or die(mysql_error());
								if(mysql_num_rows($q2) > 0) {
									$r2 = mysql_fetch_assoc($q2);

									$borrower = strtoupper($r2['lname']) . ", " .strtoupper($r2['fname']);

								} else {
									$q3 = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$r[borrower]'") or die(mysql_error());
									if(mysql_num_rows($q3) > 0) {
										$r3 = mysql_fetch_assoc($q3);
										$borrower = strtoupper($r3['lname']) . ", " .strtoupper($r3['fname']);

									} else {
										$borrower = $r['borrower'];
									}
								}
							?>
							{ recid: '<?php echo $r['code']; ?>', code: '<?php echo $r['code']; ?>', idnumber: '<?php echo $r['borrower']; ?>', borrower: '<?php echo htmlentities($borrower, ENT_QUOTES); ?>', borrow_date: '<?php echo datetime("m/j/y h:i:s a", $r['borrow_date']); ?>' },
						<?php } ?>
					],
					onSelect: function(event) {
		                w2ui['search_reprint'].toolbar.enable('print');
		            },
		            onUnselect: function(event) {
		                w2ui['search_reprint'].toolbar.disable('print');
		            },
				});
			});
		</script>

	<?php

	} else {
		echo "<div class='error_message'>Search not found!</div>";
	}
}

?>
