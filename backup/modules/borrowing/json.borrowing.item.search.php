<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

$limit = "100";
if(isset($_POST['limit'])){
    $limit = escapeString($_POST['limit']);
}

$offset = "0";
if(isset($_POST['offset'])){
    $offset = escapeString($_POST['offset']);
}

$search_field = "";
if(isset($_POST['search'][0]['field'])){
    $search_field = escapeString($_POST['search'][0]['field']);
}

$search_value = "";
if (isset($_POST['search'][0]['value'])){
    $search_value = escapeString($_POST['search'][0]['value']);
}

$q = mysql_query("SELECT barcode, description FROM inv_item WHERE phaseout = '0' AND CONCAT(barcode, description) LIKE '%$search_value%' ORDER BY description LIMIT $offset, $limit") or die(mysql_error());

$total_records = mysql_num_rows($q);
?>

<?php

echo "{ \"total\": $total_records, \"records\": [";
	$cnt = 0;
	while($r = mysql_fetch_assoc($q)) {
		$cnt++;
		if($cnt > 1) {
			echo ",";
		}
		
		$q2 = mysql_query("SELECT barcode FROM borrow_item WHERE void = '0' AND item_return = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
		if(mysql_num_rows($q2) > 0) {
			$status = "Yes";
			$style  = "background-color: #FF3333";
		} else {
			$status = "No";
			$style  = "";
		}

       	echo "{ \"recid\": \"$r[barcode]\", \"barcode\": \"$r[barcode]\", \"desc\": \"" . setUTF8String(cleanString($r['description'])) . "\", \"status\": \"$status\", \"style\": \"$style\" }";

   }

echo " ] }";

?>
