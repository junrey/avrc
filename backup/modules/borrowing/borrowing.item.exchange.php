<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(!isset($_SESSION[getSystemName()]['usercode'])) {
	echo "<div class='error_message'>$error_display_no_username</div>";
	exit();
}

if(isset($_GET["barcode"]) && trim($_GET["barcode"]) != "" && isset($_GET["new_barcode"]) && trim($_GET["new_barcode"]) != "") {
	$barcode     = escapeString($_GET['barcode']);
	$new_barcode = escapeString($_GET['new_barcode']);

	$r = query("SELECT borrow_header FROM borrow_item WHERE void = '0' AND item_return = '0' AND borrow_header <> '' AND barcode = '$barcode'");
	if(!empty($r)) {
		$borrow_header = "";
		foreach($r as $r) {
			$borrow_header = $r['borrow_header'];
		}

		try {
			begin();

			query("UPDATE borrow_item SET barcode = '$new_barcode' WHERE void = '0' AND item_return = '0' AND borrow_header = '$borrow_header' AND barcode = '$barcode'");

			$code = generateCode('EXC');

			query("INSERT INTO borrow_item_exchange (code, borrow_header, barcode_old, barcode_new, usercode, exchange_date) VALUES ('$code', '$borrow_header', '$barcode', '$new_barcode', '".getSessionVar('usercode')."', NOW())");

			commit();

		} catch(Exception $e) {
			rollback();
			exit();
		}

	?>
		<table class="tdata" style="width:100%; border:0;">
        	<thead>
                <tr>
                    <th>#</th>
                    <th>Barcode</th>
                    <th>Description</th>
                    <th>Brand</th>
                    <th>Date</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
			<?php
                $r = query("SELECT itemcode, barcode, qty, void, usercode, borrow_date FROM borrow_item WHERE void = '0' AND item_return = '0' AND borrow_header = '$borrow_header' AND barcode = '$new_barcode'");
                foreach($r as $r) {
                    $description = "";
                    $r2 = query("SELECT description, brand_code FROM $db_inventory.inv_item WHERE code = '$r[itemcode]'");
                    foreach($r2 as $r2) {
                        $description = $r2['description'];

                        $brand = "";
                        $r3 = query("SELECT detail FROM $db_inventory.inv_item_brand WHERE code = '$r2[brand_code]'");
                        foreach($r3 as $r3) {
                            $brand = $r3['detail'];
                        }
                    }
            ?>
                    <tr>
                        <td>1</td>
                        <td><?php echo $r['barcode']; ?></td>
                        <td><?php echo $description; ?></td>
                        <td><?php echo $brand; ?></td>
                        <td style="width:70px;"><?php echo datetime("m/j/y", $r['borrow_date']); ?><br /><?php echo datetime("h:i:s a", $r['borrow_date']); ?></td>
                        <td style="text-align:center;"><img src="images/tick_16.png" /></td>
                    </tr>
			<?php
				}
	}
}
?>
<input type="hidden" name="item_count" id="item_count" value="0">
<style>
	.tdata th { text-align:center; background-color:#575757; color:#fff; }
	.tdata td { vertical-align:top; }
</style>
