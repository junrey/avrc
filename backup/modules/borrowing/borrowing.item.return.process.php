<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(!isset($_SESSION[getSystemName()]['usercode'])) {
	echo "<div class='error_message'>$error_display_no_username</div>";
	exit();
}

if(isset($_GET["return_barcode"]) && trim($_GET["return_barcode"]) != "") {
	$barcode = escapeString($_GET['return_barcode']);

	$q = mysql_query("SELECT borrow_header FROM borrow_item WHERE void = '0' AND item_return = '0' AND borrow_header <> '' AND barcode = '$barcode'") or die(mysql_error());
	if(mysql_num_rows($q) > 0) {

		$r = mysql_fetch_assoc($q);

		try {
			begin();

			mysql_query("UPDATE borrow_item SET item_return = '1', a_return_date = NOW(), rusercode = '".getSessionVar('usercode')."' WHERE void = '0' AND item_return = '0' AND borrow_header = '$r[borrow_header]' AND barcode = '$barcode'") or die(mysql_error());

			commit();

		} catch(Exception $e) {
			rollback();
			exit();
		}

?>

		<script>
			$(function() {
				$().w2destroy("item");
				$('#item').w2grid({
					header: 'Return Successful',
					name: 'item',
					multiSelect : false,
					show : {
						header      : true,
						lineNumbers : true
					},
					columns: [
						{ field: 'barcode', caption: 'Barcode', size: '15%' },
						{ field: 'desc', caption: 'Description',  size: '25%'},
						{ field: 'borrow_date', caption: 'Borrow Date', size: '25%' },
						{ field: 'return_date', caption: 'Return Date', size: '25%' },
						{ field: 'status', caption: 'Status', size: '10%' }
					],
					records: [
						<?php
							$q2 = mysql_query("SELECT barcode, itemcode, usercode, borrow_date, return_date, a_return_date FROM borrow_item WHERE void = '0' AND item_return = '1' AND borrow_header = '$r[borrow_header]' AND barcode = '$barcode'") or die(mysql_error());
							$r2 = mysql_fetch_assoc($q2);

							$desc = "";
							$q3 = mysql_query("SELECT description FROM inv_item WHERE barcode = '$barcode'") or die(mysql_error());
							$r3 = mysql_fetch_assoc($q3);
							$desc = htmlentities($r3['description'], ENT_QUOTES);

						?>
						{ recid: '<?php echo $r2['barcode']; ?>', barcode: '<?php echo $r2['barcode']; ?>', desc: '<?php echo $desc; ?>', borrow_date: '<?php echo datetime("m/j/y", $r2['borrow_date']); ?> <?php echo datetime("h:i:s a", $r2['borrow_date']); ?>', return_date: '<?php echo datetime("m/j/y", $r2['return_date']); ?> <?php echo datetime("h:i:s a", $r2['return_date']); ?>', status: 'OK' },

					],
					onDblClick: function(event) {
						event.onComplete = function () {
							var sel = w2ui['item'].getSelection();
							var recid = sel[0];
							$('#barcode').val(recid).focus();
						}
					}
				});
			});
		</script>
     <?php
	}
}
?>
