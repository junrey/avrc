<?php session_start();
require_once("../../db/db.connection.php");
require_once("../../inc/inc.functions.php");

if(!isset($_SESSION[getSystemName()]['usercode'])) {
	echo "<div class='error_message'>$error_display_no_username</div>";
	exit();
}


if(isset($_POST['borrower']) && trim($_POST['borrower']) != "") {
	$borrower    = escapeString($_POST['borrower']);
	$teacher     = escapeString($_POST['teacher']);

	$return_date = trim($_POST['return_date']);

	$venue       = escapeString($_POST['venue']);
	$subject     = escapeString($_POST['subject']);
	$remarks     = escapeString($_POST['remarks']);

	$code        = generateCode('BRW');

	$total_item = 0;
	$q = mysql_query("SELECT COUNT(barcode) AS total FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."'") or die(mysql_error());
	$r = mysql_fetch_assoc($q);
	$total_item = $r['total'];

	if($total_item > 0) {
		try {
			begin();
			
			// get borrower college
			$isstudent = 1;
			$coll      = "OTHR";
			
			$q2 = mysql_query("SELECT coll FROM student WHERE idnumber = '$borrower'") or die(mysql_error());
			if(mysql_num_rows($q2) > 0) {
				$r2 = mysql_fetch_assoc($q2);
				if($r2['coll'] == "AS") {
					$coll = "CAS";
				} else if($r2['coll'] == "BE") {
					$coll = "EDU";
				} else if($r2['coll'] == "BN") {
					$coll = "BSN";
				} else if($r2['coll'] == "CO") {
					$coll = "CBA";
				} else if($r2['coll'] == "EN") {
					$coll = "ENG";
				} else if($r2['coll'] == "LAW") {
					$coll = "LAW";
				} else if($r2['coll'] == "MD") {
					$coll = "MED";
				} else if($r2['coll'] == "BL") {
					$coll = "LAW";
				} else {
					$coll = "OTHR";
				}
				
			} else {
				$q3 = mysql_query("SELECT coll FROM teacher WHERE idnumber = '$borrower'") or die(mysql_error());
				if(mysql_num_rows($q3) > 0) {
					$r3 = mysql_fetch_assoc($q3);
					$isstudent = 0;
					$coll = $r3['coll'];
				}
			}

			mysql_query("INSERT INTO borrow_header (code, usercode, borrower, borrow_date, ay, sem, term, coll, isstudent) VALUES ('$code', '".getSessionVar('usercode')."', '$borrower', NOW(), '".getSessionVar('ay')."', '".getSessionVar('sem')."', '".getSessionVar('term')."', '$coll', '$isstudent')") or die(mysql_error());
			mysql_query("UPDATE borrow_item SET borrow_header = '$code', borrower = '$borrower', venue = '$venue', subject = '$subject', remarks = '$remarks', teacher = '$teacher', return_date = '$return_date' WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."'") or die(mysql_error());

			commit();

		} catch(Exception $e) {
			rollback();
			exit();
		}

		$receipt_header = "";
		$q = mysql_query("SELECT receipt_header FROM config") or die(mysql_error());
		$r = mysql_fetch_assoc($q);
		$receipt_header = $r['receipt_header'];

?>
        <div><?php echo $receipt_header . "<br /><br />"; ?></div>
        <div>
        <?php
			setUTF8();
            $cnt = 0;
            $q = mysql_query("SELECT borrow_header, usercode, subject, venue, teacher, remarks, barcode, borrow_date, return_date FROM borrow_item WHERE void = '0' AND item_return = '0' AND borrower = '$borrower' ORDER BY borrow_date DESC") or die(mysql_error());
            while($r = mysql_fetch_assoc($q)) {
                if($cnt == 0) {
                    echo "TRANS ID: " . $r['borrow_header'] . "<br />";

                    $subject = setUTF8String($r['subject']);
                    $venue   = setUTF8String($r['venue']);
                    $teacher = setUTF8String($r['teacher']);

                    if($subject != "" && strtoupper($subject) != "NONE") { echo "SUBJECT: " . $subject . "<br />"; }
                    if($venue != "" && strtoupper($venue) != "NONE") { echo "VENUE: " . $venue . "<br />"; }
                    if($teacher != "" && strtoupper($teacher) != "NONE") { echo "TEACHER: " . $teacher . "<br />"; }

                    echo "DATE: " . datetime("m/j/y h:i:s a", $r['borrow_date']) . "<br />";
                    echo "RETURN: " . datetime("m/j/y h:i:s a", $r['return_date']) . "<br />";
                    echo "<div style='text-align:center;'>** ********** I T E M S ********** **</div><br />";
                }

                $cnt++;

                $description = "";
                $q2 = mysql_query("SELECT description FROM inv_item WHERE phaseout = '0' AND barcode = '$r[barcode]'") or die(mysql_error());
				$r2 = mysql_fetch_assoc($q2);
                $description = $r2['description'];

                echo "BARCODE: " . $r['barcode'] . "<br />";
                echo "ITEM: " . $description . "<br />";
            }

            echo "<br />";

            $q = mysql_query("SELECT lname, fname FROM student WHERE idnumber = '$borrower'") or die(mysql_error());
            if(mysql_num_rows($q) > 0) {
				$r = mysql_fetch_assoc($q);

                echo "<font color='#FF0000'>BORROWER: " . strtoupper($r['fname']  . " " . $r['lname']) . "</font><br />";

            } else {
                $q = mysql_query("SELECT lname, fname FROM teacher WHERE idnumber = '$borrower'") or die(mysql_error());
				if(mysql_num_rows($q) > 0) {
					$r = mysql_fetch_assoc($q);

                	echo "<font color='#FF0000'>BORROWER: " . strtoupper($r['fname'] . " " . $r['lname']) . "</font><br />";

                } else {
                    echo "<font color='#FF0000'>BORROWER: $borrower" . "</font><br />";
                }

            }

            echo "<font color='#FF0000'>SERVED BY: " . getSessionVar('username') . "</font><br />";
        ?>
        <br /><br />
        </div>
        <div style="text-align:center;">***************************************</div>
<?php

		/*$q = mysql_query("SELECT receipt_printing FROM config") or die(mysql_error());
		$r = mysql_fetch_assoc($q);
		if($r['receipt_printing'] == "1") {
			echo "<script>window.print();</script>";
		}*/
	}
}

   

?>
