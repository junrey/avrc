<?php session_start();
    require_once("../../db/db.connection.php");
    require_once("../../inc/inc.functions.php");

    if(!isset($_SESSION[getSystemName()]['usercode'])) {
       header ("location: login.php");
    }

    if(getSessionVar('usercode') != getRootCode()) {
        $q = mysql_query("SELECT a.usercode FROM _userprogram AS a INNER JOIN _program AS b ON a.program_code = b.code WHERE a.usercode = '".getSessionVar('usercode')."' AND b.keyword = 'circulation'") or die(mysql_error());
        if(mysql_num_rows($q) == 0) {
            header ("location: login.php");
        }
    }

    // cleaning
    // query for removing previous scan items
    $q = mysql_query("SELECT itemcode FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."' LIMIT 1") or die(mysql_error());
    if(mysql_num_rows($q) > 0) {
        try {
            begin();
            mysql_query("DELETE FROM borrow_item WHERE void = '0' AND borrow_header = '' AND usercode = '".getSessionVar('usercode')."'") or die(mysql_error());
            commit();
        } catch(Exception $e) {
            rollback();
            exit();
        }
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link type="text/css" rel="stylesheet" href="../../plugin/jquery/css/smoothness/jquery-ui-1.10.3.custom.min.css" >
<link type="text/css" rel="stylesheet" href="../../style/style.borrowing.css" />
<link type="text/css" rel="stylesheet" href="../../plugin/w2ui/w2ui-1.4.2.min.css" />
<script src="../../plugin/jquery/js/jquery-2.0.0.min.js"></script>
<script src="../../plugin/jquery/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="../../script/script.borrowing.jquery.js"></script>
<script src="../../script/script.shortcut.js"></script>
<script src="../../script/script.borrowing.js"></script>
<script src="../../plugin/w2ui/w2ui-1.4.2.min.js"></script>
<script src="../../script/script.ajax.js"></script>

<style type="text/css" media="print">
	.noprint {display:none;}
	.w2ui-popup {display:none;}
</style>

<style type="text/css" media="screen">
	.nodisplay {display:none;}
</style>
</head>
<body>
<div class="noprint" id="borrowing_panel">
    <div style="height:60px;">
        <input type="text" name="barcode" id="barcode" placeholder="Your barcode here..." required autocomplete="off" disabled="disabled"/>
        <button onclick="addItem();" title="Add item."><h1 class="dark_green">Scan</h1></button>
        <?php //<button onclick="searchItem();" title="Search item."><h1 class="green">Search</h1></button> ?>
        <button id="besc"><h1 class="red">Esc</h1></button>
    </div>
    <div style="height:430px;">
        <div style="float:left;">
            <div id="item"></div>
            <div id="borrower_name"></div>
        </div>
        <div style="float:left; background-color:#7BC147;">
            <div id="tender_panel" style="visi">
                <div>Return Date</div>
                <div><input type="text" name="datenow" id="datenow" class="datenow" readonly="readonly" placeholder="Date..." required /></div>
                <div>Venue / Location</div>
                <div><input type="text" name="venue" id="venue" tabindex="1" value="none" required autocomplete="off" onfocus="upxAlter(1); this.select();" maxlength="100"/></div>
                <div>Subject</div>
                <div><input type="text" name="subject" id="subject" tabindex="2" value="none" required autocomplete="off" onfocus="upxAlter(2); this.select();" maxlength="100"/></div>
                <div>Teacher</div>
                <div><input type="text" name="tborrower" id="tborrower" tabindex="3" autocomplete="off" onfocus="upxAlter(3); this.select();" maxlength="50"/></div>
                <div>Remarks/Comments</div>
                <div><input type="text" name="remarks" id="remarks" tabindex="4" autocomplete="off" onfocus="upxAlter(4); this.select();" maxlength="300"/></div>
                <div style="margin:0 auto; width:170px;">
                    <button onclick="tenderEnter();">
                        <h1 class="dark_green">Enter</h1>
                    </button>
                </div>
            </div>
        </div>
    </div>
	<div>
		<button id="bspace" onclick="newTrans();"><h1 class="dark_green">[ Space ] New</h1></button>
		<button id="bf10" onclick="tender();"><h1 class="dark_green">F10 Checkout</h1></button>
		<button id="bf12" onclick="returnItem();"><h1 class="dark_green">F12 Return</h1></button>
		<button id="bf1" onclick="reprint();"><h1 class="green">F1 Re-Print</h1></button>
		<button id="bf7" onclick="voidAll();"><h1 class="red">F7 Remove All</h1></button>
		<button id="bf8" onclick="voidItem();"><h1 class="red">F8 Remove</h1></button>
	</div>
</div>

<div id="print_panel" class="nodisplay"></div>

<div id="window_newTrans">
	<div style="height:50px;">
    	<input type="text" name="borrower" id="borrower" required placeholder="ID Number, Last Name." title="Search ID Number, Last Name." />
        <button onclick="newTransEnter();"><h1 class="dark_green">Enter ID</h1></button>
        <button onclick="searchBorrower();"><h1 class="green">Search</h1></button>
        <button onclick="newTransCancel();"><h1 class="red">Esc</h1></button>
    </div>
    <div id="search_borrower"></div>
</div>

<div id="window_reprint">
    <div style="height:50px;">
        <input type="text" name="reprint_trans_id" id="reprint_trans_id" placeholder="Transaction ID" style="width:150px;" />
        <input type="text" name="reprint_username" id="reprint_username" placeholder="Borrower ID" style="width:130px;" />
        <input type="text" name="reprint_date" id="reprint_date" class="datenow" placeholder="Date..." readonly style="width:130px;" />
        <button onclick="reprintSearch();"><h1 class="green">Search</h1></button>
        <button onclick="reprintCancel();"><h1 class="red">Esc</h1></button>
    </div>
    <div id="search_reprint"></div>
</div>

<div id="window_item_search"></div>

<div id="loading_status" class="noprint"></div>
<?php

    // set printing availability
    $receipt_printing = "";
    $q = mysql_query("SELECT receipt_printing FROM config") or die(mysql_error());
    $r = mysql_fetch_assoc($q);
    $receipt_printing = $r['receipt_printing'];

?>
<input type="hidden" name="receipt_printing" id="receipt_printing" value="<?php echo $receipt_printing; ?>" />
<script>
$(function() {
	$('button').button();
    $('.datenow').datepicker({ dateFormat: 'yy-mm-dd' });

});
</script>
