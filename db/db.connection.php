<?php
// Make a MySQL Connection
$host           = "localhost";
$user           = "root";
$password       = "";
$db_circulation = "circulation";
$db_inventory   = "inventory";
$db_avrc        = "avrc"; 

mysql_connect($host, $user, $password);
mysql_select_db($db_avrc);

ini_set('memory_limit', '60M');
set_time_limit(0);
date_default_timezone_set('Asia/Manila');

mysql_query("SET AUTOCOMMIT=0");

function begin(){
	mysql_query("START TRANSACTION");
}
function commit(){
	mysql_query("COMMIT");
}
function rollback(){
	mysql_query("ROLLBACK");
}
function errorMsg() {
	//mysql_error();
}
function setUTF8() {
	mysql_query("SET NAMES utf8"); //display enye
}
function query($s) {
	$q = mysql_query($s) or die(mysql_error());
	$a = array();
	
	if($q && !is_bool($q)) {
		setUTF8();
		while($r = mysql_fetch_assoc($q)){
			$a[]=$r;
		}
		mysql_free_result($q);
		
	}
	return $a;
}
function queryInnoDB($s) {
	try { 			
		begin();
		query($s);
		commit(); 	
	} catch(Exception $e) { 
		rollback(); 
		exit();
	}
}
?>
